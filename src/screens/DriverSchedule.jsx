import React, { useContext, useEffect, useState } from 'react';
import { View, StyleSheet, Alert } from 'react-native';
import { Radio, Text, Container, Box, ScrollView, useToast } from 'native-base';
import { Pressable } from 'react-native';
import { withTranslation } from 'react-i18next';
import { Button } from 'src/components/molecules/Button';
import PropTypes from 'prop-types';
import { AppContext } from '../store';
import { getDriverSchedule, updateDriverSchedule } from '../api/driver';
import { showApiErrorToast, ucFirst } from '../utils';

// const workDays = [
//   { day: 'Monday', timing1: true, timing2: false, timing3: false },
//   { day: 'Tuesday', timing1: false, timing2: false, timing3: false },
//   { day: 'Wednesday', timing1: false, timing2: true, timing3: false },
//   { day: 'Thursday', timing1: false, timing2: false, timing3: false },
//   { day: 'Friday', timing1: false, timing2: false, timing3: false },
//   { day: 'Saturday', timing1: false, timing2: false, timing3: false },
// ]

const WEEK_DAYS = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
const SHIFTS = ['7-12', '12-1', 'All day'];

function DriverSchedule({ t, navigation }) {
  const obj = {};
  WEEK_DAYS.map((i) => {
    obj[i] = null;
  });

  const [schedule, setSchedule] = useState(obj);
  const context = useContext(AppContext);
  const toast = useToast();

  async function fetchData() {
    try {
      const { driver_schedule } = await getDriverSchedule();
      if (driver_schedule) setSchedule(driver_schedule);
    } catch (e) {
      // console.error(e);
    }
  }

  useEffect(() => {
    fetchData();
    const unsubListener = navigation.addListener('beforeRemove', (e) => {
      if (context.driverSchedule) {
        return;
      }
      e.preventDefault();

      Alert.alert(t('Work_schedule_required'), t('work_schedule_validation'), [{ text: 'OK' }]);
    });
    return unsubListener;
  }, [navigation, context]);

  const validate = (data) => {
    let blanks = 0;
    for (const k of Object.keys(data)) if (!data[k]) blanks++;
    return blanks <= 2;
  };

  const handleRadioClick = (day, time) => {
    if (schedule[day] === time) setSchedule({ ...schedule, [day]: null });
    else setSchedule({ ...schedule, [day]: time });
  };

  const handleSubmit = async () => {
    if (validate(schedule)) {
      context.setLoading(true);
      await updateDriverSchedule(schedule);
      try {
        await context.setDriverSchedule(schedule);
        setTimeout(() => {
          navigation.goBack();
        }, 1000);
      } catch (err) {
        showApiErrorToast(err, toast);
      } finally {
        context.setLoading(false);
      }
    }
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      {WEEK_DAYS.map((day) => (
        <View key={day} style={styles.schCard}>
          <Text style={{ fontWeight: '600', marginTop: 5, fontSize: 16 }}>{ucFirst(day)}</Text>
          <View style={styles.strike} />
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '100%',
              margin: 12,
            }}
          >
            {SHIFTS.map((time) => (
              <Pressable
                key={time}
                onPress={() => {
                  handleRadioClick(day, time);
                }}
              >
                <Radio.Group
                  pointerEvents='none'
                  value={schedule[day]}
                  style={{ flexDirection: 'row', marginLeft: 12, marginRight: 12 }}
                >
                  <Radio aria-label={time} color={'#23619C'} selectedColor={'#23619C'} value={time}>
                    {time}
                  </Radio>
                </Radio.Group>
              </Pressable>
            ))}
          </View>
        </View>
      ))}
      <Button text={t('continue')} style={styles.saveButton} disabled={!validate(schedule)} onPress={handleSubmit} />
    </ScrollView>
  );
}

DriverSchedule.propTypes = {
  t: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
};

const styles = StyleSheet.create({
  schCard: {
    margin: 6,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    borderRadius: 18,
    shadowOffset: { width: 0.95, height: 0.95 },
    shadowColor: '#a2a2a2',
    shadowOpacity: 0.5,
    shadowRadius: 2,
    marginLeft: 15,
    marginRight: 15,
  },
  container: {
    width: '100%',
    marginTop: 4,
    backgroundColor: '#f4f4f4',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  strike: {
    height: 2,
    width: '30%',
    backgroundColor: '#0b5ea2',
  },
  saveButton: {
    color: '#fff',
    margin: 12,
  },
});

export default withTranslation()(DriverSchedule);
