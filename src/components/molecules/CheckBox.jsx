import React from 'react';
import PropTypes from 'prop-types';

import { ListItem, Text, CheckBox as C, Body } from 'native-base';

const CheckBox = ({ title, checked, onPress, ...props }) => (
  <ListItem onPress={onPress}>
    <C checked={checked} {...props} />
    <Body>
      <Text>{title}</Text>
    </Body>
  </ListItem>
);

CheckBox.propTypes = {
  title: PropTypes.string,
  checked: PropTypes.boolean,
  onPress: PropTypes.func,
};

export { CheckBox };
