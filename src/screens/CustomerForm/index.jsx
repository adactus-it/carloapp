import React, { useCallback, useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import { Box, ScrollView, useToast, Text } from 'native-base';
import { SwitchComponent } from 'src/components/molecules';
import { CarIssues, CarPhotos, CustomerSignature } from 'src/components/organisms';
import CarDetails from './CarDetails';

import { AppContext } from 'src/store';
import { ConfirmationDialog } from 'src/components/organisms';
import {
  getCustomerFormForOrder,
  updateCustomerFormCarIssues,
  updateCustomerFormForOrder,
  updateCustomerFormMedia,
  getCustomerFormPdf,
} from 'src/api/customer-forms';
import { showApiErrorToast } from '../../utils';
import { completeOrder } from '../../api/orders';
import InfoContainer from '../../components/molecules/InfoContainer';

const headings = ['Basic Information', 'Issues in car', 'Car Photos'];

function CustomerForm({ navigation, route }) {
  const numberOfForms = 4;

  const [formStates, setFormStates] = useState({});
  const [activeForm, setActiveForm] = useState(null);
  const [currentFormId, setCurrentFormId] = useState(1);
  const [pdfUrl, setPdfUrl] = useState();
  const stages = ['info', 'car_issues', 'car_photos', 'signature'];


  const appendToFormState = (obj) => {
    setFormStates({ ...formStates, ...obj });
  };
  const { forms, setLoading } = useContext(AppContext);
  const toast = useToast();

  const { orderId } = route?.params;

  const saveOrder = () => {
    forms.update(orderId, { formStates, activeForm });
  };
  const fetchData = useCallback(async () => {
    try {
      setLoading(true);
      const res = await getCustomerFormForOrder(orderId);
      if (res && res.body) {
        const {
          customer_form,
          customer_form: { active_stage, id },
        } = res.body;
        const stage = stages.indexOf(active_stage);
        const setting = stage >= 0 ? stage + 1 : 1;
        setActiveForm(setting);
        setFormStates(customer_form);
        setCurrentFormId(id);
      }
    } catch (err) {
    } finally {
      setLoading(false);
    }
  }, [orderId]);

  useEffect(() => {
    fetchData();
    fetchPdfUrl();
  }, []);

  const fetchPdfUrl = async () => {
    const pdfRes = await getCustomerFormPdf(orderId);
    if (pdfRes && pdfRes.body) {
      const { pdf_url } = pdfRes.body;
      setPdfUrl(pdf_url);
      console.log({ pdf_url });
    }
  };

  function moveToNext() {
    if (activeForm < numberOfForms) {
      setActiveForm(activeForm + 1);
    } else {
      ConfirmationDialog(
        'Copy will be emailed to the customer',
        openCarReturnDialog,
        'Does the customer need a copy of the form?',
        '',
        '',
        openCarReturnDialog,
      );
    }
  }

  const openCarReturnDialog = () => {
    ConfirmationDialog(
      '',
      () => {
        navigation.replace('CarReturn', { orderId: orderId });
      },
      'Does the customer have a car to return?',
      '',
      '',
      () => {
        completeOrder(orderId);
        navigation.reset({
          index: 0,
          routes: [{ name: 'HomeScreen' }],
        });
      },
    );
  };

  async function handleCarIssuesSubmit(car_issues) {
    setLoading(true);
    if (car_issues.length < 1) {
      return moveToNext();
    }
    try {
      await updateCustomerFormCarIssues(currentFormId, { car_issues });
      saveOrder();
      moveToNext();
    } catch (error) {
      const message = error?.data?.message || error?.message || 'Connection error';
      message && toast.show({ title: message });
    } finally {
      setLoading(false);
    }
  }

  async function handleCarMediaSubmit(photos) {
    try {
      setLoading(true);
      for (const it of photos) {
        await updateCustomerFormMedia(currentFormId, it);
      }
      saveOrder();
      moveToNext();
    } catch (error) {
      // console.error({ error });
      showApiErrorToast(error, toast);
    } finally {
      setLoading(false);
    }
  }

  async function handleSignatureSubmit(signature) {
    try {
      setLoading(true);
      await updateCustomerFormForOrder(orderId, { ...formStates, signature });
      saveOrder();
      moveToNext();
    } catch (error) {
      showApiErrorToast(error, toast);
    } finally {
      setLoading(false);
    }
  }

  async function onSubmit(data) {
    try {
      setLoading(true);
      appendToFormState(data);
      const response = await updateCustomerFormForOrder(orderId, { ...formStates, ...data });
      const {
        body: {
          customer_form: { id },
        },
      } = response;
      setCurrentFormId(id);

      saveOrder();

      moveToNext();
    } catch (error) {
      // console.error({ error });
      showApiErrorToast(error, toast);
    } finally {
      setLoading(false);
    }
  }

  return (
    <ScrollView
      contentContainerStyle={{
        minHeight: '100%',
      }}
    >
      <Box>
        <Text fontSize='2xl' style={{ textAlign: 'center', marginTop: 20 }}>
          {headings[activeForm - 1]}
        </Text>
      </Box>
      <Box>
        <SwitchComponent active={activeForm}>
          <CarDetails data={formStates} key={1} onSubmit={onSubmit} />
          <CarIssues data={formStates} key={2} onSubmit={handleCarIssuesSubmit} />
          <CarPhotos data={formStates} key={3} onSubmit={handleCarMediaSubmit} />
          <CustomerSignature pdfUrl={pdfUrl} data={formStates} key={4} onSubmit={handleSignatureSubmit} />
        </SwitchComponent>
      </Box>
    </ScrollView>
  );
}

CustomerForm.propTypes = {
  navigation: PropTypes.object,
  route: PropTypes.object.isRequired,
};

export default withTranslation()(CustomerForm);
