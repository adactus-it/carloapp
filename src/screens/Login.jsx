import React, { useContext, useState, useRef, useEffect } from 'react';
import { StyleSheet, Image } from 'react-native';
import { Box, Text, VStack, ScrollView, useToast } from 'native-base';
import { withTranslation } from 'react-i18next';
import CarLogo from '../assets/logo.png';
import PropTypes from 'prop-types';
import { Button, FormControl } from 'src/components/molecules';
import { AppContext } from 'src/store';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { login } from '../api/auth';
import { showApiErrorToast } from '../utils';

const schema = yup.object().shape({
  email: yup.string().email().required(),
  password: yup.string().min(6).required(),
});

const Login = ({ t }) => {
  const context = useContext(AppContext);
  const [show, setShow] = useState(false);

  const setLoading = context.setLoading;

  const toast = useToast();

  const passwordRef = useRef();

  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
    defaultValues: {
      email: '',
      password: '',
    },
  });

  async function authenticate(data) {
    try {
      setLoading(true);
      const res = await login(data);
      if (!res || !res.body) {
        throw new Error('No response from server');
      }

      const { user, token } = res.body;
      user.name = [user.first_name, user.last_name].join(' ');
      context.login(user, token);
    } catch (error) {
      // console.error({ error });
      showApiErrorToast(error, toast);
    } finally {
      setLoading(false);
    }
  }

  return (
    <ScrollView
      _contentContainerStyle={{
        px: '44px',
        w: '100%',
      }}
    >
      <VStack display='flex' alignItems='center' h='100%' w='100%'>
        {/* Language Selector */}
        {/* <View style={{ marginTop: 20, flexDirection: 'row', alignItems: 'center' }}>
          <SubText text={t('current_lang') + ':'} />
          <Picker
            mode='dropdown'
            iosIcon={<Icon name='arrow-down' />}
            placeholderStyle={{ color: '#bfc6ea' }}
            placeholderIconColor='#007aff'
            selectedValue={i18n.language}
            onValueChange={(val) => i18n.changeLanguage(val)}
          >
            <Picker.Item label={'English'} value='en' />
            <Picker.Item label='German' value='de' />
          </Picker>
        </View> */}
        {/*End  Language Selector */}

        <Image style={{ marginTop: 100 }} source={CarLogo} resizeMode='contain' />
        <Box style={{ marginTop: 40 }} />
        <Box>
          <Text style={{ fontWeight: '700', fontSize: 30 }}>{t('welcome')}</Text>
          <Text style={{ fontWeight: '600', fontSize: 18, marginTop: 5 }}>{t('confirm')}</Text>
        </Box>
        <Box m={4} w={280}>
          <FormControl
            block
            label={t('Email Address')}
            autoCapitalize='none'
            placeholder={t('email')}
            textContentType='emailAddress'
            onSubmitEditing={() => passwordRef?.current?.focus()}
            returnKeyType='next'
            control={control}
            name='email'
            isRequired={true}
            error={errors?.email}
            rules={{ required: t('email_required'), minLength: 6 }}
          />
          <FormControl
            block
            autoCapitalize='none'
            forwardedRef={passwordRef}
            label={t('password')}
            isRequired={true}
            placeholder={t('password')}
            control={control}
            name='password'
            rules={{ required: 'Password is required', minLength: 6 }}
            type={show ? 'text' : 'password'}
            error={errors?.password}
            InputRightElement={
              <Button
                ml={1}
                roundedLeft={0}
                roundedRight='md'
                onPress={() => setShow(!show)}
                text={show ? 'Hide' : 'Show'}
              />
            }
            onSubmitEditing={handleSubmit(authenticate)}
          />
        </Box>
        <Button w={280} onPress={handleSubmit(authenticate)} style={styles.saveButton} text={t('sign_in')} />
        {/*<View style={{ marginTop: 20, flexDirection: 'row', alignItems: 'center' }}>*/}
        {/*  <Text>{t('forgot_password')}</Text>*/}
        {/*  <TouchableOpacity>*/}
        {/*    <Text style={{ color: 'red', marginLeft: 4 }}>{t('retrieve')}</Text>*/}
        {/*  </TouchableOpacity>*/}
        {/*</View>*/}
      </VStack>
    </ScrollView>
  );
};

Login.propTypes = {
  t: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
};

const styles = StyleSheet.create({
  saveButton: {
    // height: 45,
    color: '#fff',
    backgroundColor: '#23619C',
    borderRadius: 10,
    marginTop: 20,
  },
});

export default withTranslation()(Login);
