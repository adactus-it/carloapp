import React, { useState, useContext, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import { ScrollView, Box, useToast, Text } from 'native-base';
import { SwitchComponent } from 'src/components/molecules';
import CarDetails from './CarDetails';
import CarDamage from './CarDamage';
import VisualInspection from './VisualInspection';

import { AppContext } from 'src/store';
import { CarIssues, CarPhotos, CustomerSignature } from 'src/components/organisms';
import {
  getCarReturnFormForOrder,
  updateCarReturnFormCarIssues,
  updateCarReturnFormForOrder,
  updateCarReturnFormMedia,
} from 'src/api/car-return-forms';
import { showApiErrorToast } from 'src/utils';
import { ROUTES } from 'src/utils';
import InspectionDetails from './InspectionDetails';
import ReceiptReturn from './ReceiptReturn';
import { getCarReturnFormPdf, updateCarReturnTechnicalDefects } from '../../api/car-return-forms';
import InfoContainer from '../../components/molecules/InfoContainer';

const headings = [
  'Basic Information',
  'Visual Inspection',
  'Issues in car',
  'Inner space condition',
  '',
  'Car Photos',
  'Reciept Return',
  '',
];

function CustomerForm({ navigation, route }) {
  const numberOfForms = 8;

  const [order, setOrder] = useState({});
  const [formStates, setFormStates] = useState({});
  const [activeForm, setActiveForm] = useState(null);
  const [techDefects, setTechDefects] = useState();
  const [currentFormId, setCurrentFormId] = useState(1);
  const [pdfUrl, setPdfUrl] = useState();

  useEffect(() => {
    if (activeForm === 6 && pdfUrl) {
      navigation?.setOptions({
        headerRight: () => InfoContainer(pdfUrl),
      });
    }
  }, [activeForm, pdfUrl]);

  const appendToFormState = (obj) => {
    setFormStates({ ...formStates, ...obj });
  };
  const { orderId } = route?.params;
  const { forms, setLoading } = useContext(AppContext);

  const stages = ['info', 'visual_inspection', 'car_issues', 'technical_defects', 'signature', 'car_photos', 'inner_space', 'signature_2'];

  const toast = useToast();

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const res = await getCarReturnFormForOrder(orderId);
        if (res?.body) {
          const {
            car_return_form,
            order,
            car_return_form: { active_stage, id },
          } = res.body;
          setOrder(order);
          const stage = stages.indexOf(active_stage);
          const setting = stage >= 0 ? stage + 1 : 1;
          console.log({ setting });
          setActiveForm(setting);
          delete car_return_form.signature;
          setFormStates(car_return_form);
          console.log({ car_return_form });
          setCurrentFormId(id);
        }
      } catch (err) {
        showApiErrorToast(err, toast);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
    // fetchPdfUrl()
  }, [orderId]);

  const saveOrder = () => {
    forms.update(orderId, { formStates, activeForm });
  };

  const fetchPdfUrl = async () => {
    const pdfRes = await getCarReturnFormPdf(orderId);
    if (pdfRes && pdfRes.body) {
      const { pdf_url } = pdfRes.body;
      setPdfUrl(pdf_url);
    }
  };

  function moveToNext() {
    if (activeForm < numberOfForms) {
      setActiveForm(activeForm + 1);
    } else {
      navigation.replace(ROUTES.home);
    }
  }

  async function handleCarIssuesSubmit(car_issues) {
    if (car_issues.length < 1) {
      return moveToNext();
    }
    setLoading(true);
    try {
      await updateCarReturnFormCarIssues(currentFormId, { car_issues });
      saveOrder();
      moveToNext();
    } catch (error) {
      const message = error?.data?.message || error?.message || 'Connection error';
      message && toast.show({ title: message });
    } finally {
      setLoading(false);
    }
  }

  async function handleCarMediaSubmit(photos) {
    try {
      setLoading(true);
      for (const it of photos) {
        await updateCarReturnFormMedia(currentFormId, it);
      }
      saveOrder();
      moveToNext();
    } catch (error) {
      showApiErrorToast(error, toast);
    } finally {
      setLoading(false);
    }
  }

  async function handleSignatureSubmit(signature) {
    try {
      setLoading(true);
      await updateCarReturnFormForOrder(orderId, { ...formStates, signature });
      saveOrder();
      moveToNext();
    } catch (error) {
      showApiErrorToast(error, toast);
    } finally {
      setLoading(false);
    }
  }

  async function handleSignature2Submit(signature_customer) {
    try {
      setLoading(true);
      await updateCarReturnFormForOrder(orderId, { ...formStates, signature_customer });
      saveOrder();
      moveToNext();
      toast.show({ title: 'Order completed' });
    } catch (error) {
      showApiErrorToast(error, toast);
    } finally {
      setLoading(false);
    }
  }

  async function onSubmit(data) {
    try {
      setLoading(true);
      appendToFormState(data);
      const response = await updateCarReturnFormForOrder(orderId, { ...formStates, ...data });
      const {
        body: {
          car_return_form: { id },
        },
      } = response;
      setCurrentFormId(id);
      saveOrder();

      moveToNext();
    } catch (error) {
      console.error({ error });
      showApiErrorToast(error, toast);
    } finally {
      setLoading(false);
    }
  }

  async function updateTechnicalDefects(data) {
    try {
      setLoading(true);
      appendToFormState(data);
      const response = await updateCarReturnTechnicalDefects(orderId, data);
      // const {
      //   body: {
      //     car_return_form: { id },
      //   },
      // } = response;
      // setCurrentFormId(id);
      saveOrder();

      moveToNext();
    } catch (error) {
      // console.error({ error });
      showApiErrorToast(error, toast);
    } finally {
      setLoading(false);
    }
  }

  const skipForm = () => {
    saveOrder();
    moveToNext();
  };


  return (
    <ScrollView contentContainerStyle={{}}>
      <Box>
        <Text fontSize='2xl' style={{ textAlign: 'center', marginTop: 20 }}>
          {headings[activeForm - 1]}
        </Text>
      </Box>
      <Box>
        <SwitchComponent active={activeForm}>
          <CarDetails order={order} data={formStates} key={1} onSubmit={onSubmit} />
          <VisualInspection data={formStates} key={2} onSubmit={onSubmit} />
          <CarIssues data={formStates} key={3} onSubmit={handleCarIssuesSubmit} />
          <InspectionDetails data={formStates} key={4} skipThisForm={skipForm} onSubmit={updateTechnicalDefects} />
          <CustomerSignature data={formStates} key={5} onSubmit={handleSignatureSubmit} />
          <CarPhotos data={formStates} key={6} onSubmit={handleCarMediaSubmit} />
          <ReceiptReturn data={formStates} key={7} onSubmit={onSubmit} />
          <CustomerSignature data={formStates} key={8} onSubmit={handleSignature2Submit} />
        </SwitchComponent>
      </Box>
    </ScrollView>
  );
}

CustomerForm.propTypes = {
  navigation: PropTypes.object,
  route: {
    params: PropTypes.object,
  },
};

CustomerForm.defaultProps = {
  id: '1',
};

export default withTranslation()(CustomerForm);
