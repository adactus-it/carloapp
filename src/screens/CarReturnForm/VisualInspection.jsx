import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Text, VStack, HStack, Radio } from 'native-base';
import { Button, FormControl } from 'src/components/molecules';
import { useNavigation } from '@react-navigation/native';
import { withTranslation } from 'react-i18next';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';

const OPTIONS = ['Pollution of the vehicle', 'Darkness', 'Rain / wet / snow / ice', 'Underground car park'];


const validationSchema = yup.object().shape({
  visual_inspection_done: yup.boolean(),
  visual_inspection_cancelled_reason: yup
    .string()
    .when('visual_inspection_done', {
      is: false,
      then: yup.string().required('Must enter cancellation reason'),
    }),
});

function VisualInspection({ onSubmit }) {
  const [inspection, setInspection] = useState(true);

  const { control, formStates: errors, handleSubmit } = useForm({
    resolver: yupResolver(validationSchema),
    defaultValues: {
      visual_inspection_done: inspection,
      visual_inspection_cancellation_reason: '',
    },
  });

  const navigation = useNavigation();
  return (
    <VStack space={4} justifyContent='center' alignItems={'center'} mt={10} w='100%'>
      <Text>Is the car&apos;s visual inspection carried out or not?</Text>
      <FormControl block control={control} name={'visual_inspection_done'} error={errors?.visual_inspection_done}>
        {({ field: { onChange, value } }) => <Radio.Group
          onChange={(val) => {
            setInspection(val);
            onChange(val);
          }
          }
          value={value}
          flexDirection='row'
          justifyContent='space-around'
          w='100%'
        >
          <Radio accessibilityLabel='yes' value={true}>
            Yes
          </Radio>
          <Radio accessibilityLabel='no' value={false}>
            No
          </Radio>
        </Radio.Group>}
      </FormControl>
      {inspection ? <></> : <FormControl
        block
        control={control}
        name={'visual_inspection_cancelled_reason'}
        options={OPTIONS}
        type='select'
        label='Please select any one reason below'
        placeholder='Select'
        error={errors?.visual_inspection_cancelled_reason}
      />}
      <HStack justifyContent='space-around' w='100%' my={4}>
        <Button
          text={('Save Order')}
          onPress={() => {
            navigation.navigate('HomeScreen');
          }}
        />
        <Button
          text={('Continue')}
          onPress={handleSubmit(onSubmit)}
        />
      </HStack>
    </VStack>
  );
}

VisualInspection.propTypes = {
  onSubmit: PropTypes.func,
};
export default withTranslation()(VisualInspection);
