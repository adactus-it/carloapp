/**
 * Metro configuration for React Native
 * https://github.com/facebook/react-native
 *
 * @format
 */


// eslint-disable-next-line no-undef
const { getDefaultConfig } = require('metro-config');
// eslint-disable-next-line no-undef
module.exports = (async () => {
  const {
    resolver: { sourceExts },
  } = await getDefaultConfig();
  return { resolver: { sourceExts: [...sourceExts, 'jsx'] } };
})();
// module.exports = {
//   resolver: {
//     sourceExts: ['jsx', 'js']
//   },
//   transformer: {
//     getTransformOptions: async () => ({
//       transform: {
//         experimentalImportSupport: false,
//         inlineRequires: false,
//       },
//     }),
//   },
// }
