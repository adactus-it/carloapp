import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

import { storeData, getData, clearStorage } from 'src/utils/storage';

const AppContext = React.createContext({});

const store = {
  auth: null,
  token: '',
  driverSchedule: {},
  forms: [
    {
      formStates: {},
      activeForm: '',
    },
  ],
  loading: false,
};

function AppContextProvider({ children }) {
  const [state, setState] = useState({});

  const persistState = () => {
    const { auth, token, driverSchedule, forms } = state;
    storeData('state', {
      auth,
      token,
      driverSchedule,
      forms,
    });
  };

  const methods = {
    login: (user, token) => {
      setState((state) => ({ ...state, auth: user, token }));
      storeData('auth_token', token);
      persistState();
    },
    logout: () => {
      setState(store);
      clearStorage();
    },
    getUser: () => ({ user: state.auth || {} }),
    getToken: () => state.token,
    isSignedIn: () => {
      return !!state.token;
    },
    showScheduleModal: () => !state.driverSchedule,
    setDriverSchedule: (driverSchedule) => {
      setState({ ...state, driverSchedule });
    },
    setLoading: (loading) => {
      setState((state) => ({ ...state, loading }));
    },
    forms: {
      get: (id) => state.forms[id],
      getAll: () => state.forms,
      set: (forms) => {
        const updatedForms = state.forms;
        for (const x of forms) {
          updatedForms[x.id].formStates = x;
          if (!updatedForms[x.id]) updatedForms[x.id].activeForm = 'first';
        }
        setState({ ...state, forms: updatedForms });
      },
      update: (id, { formStates, activeForm }) => {
        setState({ ...state, forms: { ...state.forms, [id]: { formStates, activeForm } } });
      },
    },
  };

  const isFirstRun = useRef(true);

  useEffect(() => {
    if (isFirstRun.current) {
      getData('state').then((state) => {
        setState(state || store);
        isFirstRun.current = false;
      });
      return;
    }
    persistState();
  }, [state]);

  return <AppContext.Provider value={{ ...state, ...methods }}>{children}</AppContext.Provider>;
}

AppContextProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export { AppContext, AppContextProvider };
