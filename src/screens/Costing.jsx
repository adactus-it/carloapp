import React, { useState, useEffect, useContext } from 'react';
import { View, StyleSheet, FlatList, Image, Alert } from 'react-native';
import PropTypes from 'prop-types';
import Modal from 'react-native-modalbox';
import ImagePicker from 'react-native-image-crop-picker';
import { Button, FormControl } from 'src/components/molecules';
import { CostingCard } from '../components/organisms';
import { IconButton, AddIcon, VStack, HStack, Box, ScrollView, useToast, Text } from 'native-base';
import { showApiErrorToast } from 'src/utils';
import { ConfirmationDialog } from 'src/components/organisms';
import { createCostingForOrder, deleteCostingForOrder, getCostingForOrder } from '../api/costings';
import { AppContext } from '../store';
import { useForm } from 'react-hook-form';

const Costing = ({ navigation, route }) => {
  const [isOpen, setModalVisib] = useState(false);
  const [orderCostings, setOrderCosting] = useState([]);

  const { loading, setLoading } = useContext(AppContext);
  const toast = useToast();

  const [receipt, setReceipt] = useState(null);

  const { orderId } = route.params;

  const {
    control,
    formState: { errors },
    handleSubmit,
  } = useForm();

  async function fetchCosting() {
    try {
      const response = await getCostingForOrder(orderId);
      if (!response?.body) throw new Error('No response');

      setOrderCosting(response.body.costing);
    } catch (error) {
      showApiErrorToast(error, toast);
    } finally {
      setLoading(false);
    }
  }

  async function submitCosting(values) {
    try {
      const { name, amount } = values;
      if (!receipt) throw new Error('Receipt is required');

      setLoading(true);
      const newCosting = {
        name,
        amount,
        receipt,
      };
      await createCostingForOrder(orderId, newCosting);
      toast.show({
        title: 'Added',
        status: 'success',
      });
      setOrderCosting([...orderCostings, newCosting]);
      setModalVisib(false);
    } catch (err) {
      err?.data?.message && toast.show({ title: err.data.message, status: 'error' });
    } finally {
      setLoading(false);
    }
  }

  async function deleteCosting(costingId) {
    try {
      setLoading(true);
      await deleteCostingForOrder(costingId);
      toast.show({ title: 'Deleted' });
      setOrderCosting(orderCostings.filter((it) => it.id !== costingId));
    } catch (err) {
      showApiErrorToast(err, toast);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <IconButton onPress={() => setModalVisib(true)} transparent icon={<AddIcon color='#fff' size='sm' />} />
      ),
    });
    fetchCosting();
  }, [navigation]);

  const pickSingleWithCamera = (cropping = true, mediaType = 'photo') => {
    ImagePicker.openCamera({
      cropping: cropping,
      width: 500,
      height: 500,
      includeExif: true,
      mediaType,
    })
      .then((image) => {
        setReceipt({
          name: image.filename || `receipt.${image.path.substr(image.path.indexOf('.'))}`,
          uri: image.path,
          type: image.mime,
        });
      })
      .catch((e) => alert(e));
  };
  const pickSingle = (cropit = true, circular = false) => {
    ImagePicker.openPicker({
      width: 500,
      height: 500,
      cropping: cropit,
      cropperCircleOverlay: circular,
      sortOrder: 'none',
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      compressVideoPreset: 'MediumQuality',
      includeExif: true,
      cropperStatusBarColor: 'white',
      cropperToolbarColor: 'white',
      cropperActiveWidgetColor: 'white',
      cropperToolbarWidgetColor: '#3498DB',
    })
      .then((image) => {
        setReceipt({
          name: image.filename || `receipt.${image.path.substr(image.path.indexOf('.'))}`,
          uri: image.path,
          type: image.mime,
        });
      })
      .catch((e) => {
        Alert.alert(e.message ? e.message : e);
      });
  };

  return (
    <Box>
      {/*<Header
        containerStyle={{ backgroundColor: '#FFF' }}
        leftComponent={_backIcon}
        centerComponent={{ text: 'COSTING', style: { color: '#000', fontSize: 20, marginBottom: 10 } }}
        rightComponent={
          <EntypoIcon
            onPress={() => setModalVisib(true)}
            style={{ marginBottom: 14 }}
            name={'plus'}
            size={24}
            color={'#000'}
          />
        }
      />*/}
      <View style={styles.container}>
        <FlatList
          data={orderCostings}
          numColumns={1}
          ListEmptyComponent={
            <Text alignSelf={'center'} mt={10}>
              Nothing to show here...
            </Text>
          }
          ListFooterComponent={() => <View style={{ height: 250 }}></View>}
          showsVerticalScrollIndicator={false}
          renderItem={({ item, index }) => {
            return (
              <CostingCard
                costing={item}
                onDelete={() => {
                  ConfirmationDialog('Are you sure you want to delete this costing?', () => {
                    deleteCosting(item.id);
                  });
                }}
                key={index}
              />
            );
          }}
          keyExtractor={(costing) => costing.id}
        />
      </View>
      <Modal
        isOpen={isOpen}
        onClosed={() => setModalVisib(false)}
        style={styles.modal}
        position={'center'}
        backdropPressToClose={false}
      >
        <ScrollView>
          <VStack style={styles.popupWindow}>
            <Box style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center' }}>
              <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 16, marginTop: 10 }}>Add Costing</Text>
            </Box>
            <Box
              style={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                width: 250,
                marginTop: 20,
              }}
            >
              <FormControl block control={control} name={'name'} label='Name' error={errors?.name} />
              <Box style={{ marginTop: 20 }}>
                <FormControl
                  block
                  error={errors?.amount}
                  control={control}
                  name={'amount'}
                  label='Amount'
                  keyboardType='numeric'
                />
              </Box>
              {receipt ? (
                <Image key={receipt.uri} style={styles.imageStyle} source={receipt} />
              ) : (
                <Box p={4} justifyContent='center' alignItems='center'>
                  <Text>{'Please upload receipt'}</Text>
                </Box>
              )}

              <HStack space={1} w='100%' justifyContent='space-around'>
                <Button
                  variant='outline'
                  borderColor='#23619C'
                  onPress={() => pickSingleWithCamera()}
                  iconLeft
                  text={'From camera'}
                  _hover={{ bg: '#EEE' }}
                />
                <Button
                  variant='outline'
                  borderColor='#23619C'
                  style={{ color: '#fff' }}
                  onPress={() => pickSingle()}
                  type={'outline'}
                  text='From gallery'
                  _hover={{ backgroundColor: '#EEE' }}
                />
              </HStack>
            </Box>
          </VStack>
          <Button loading={loading} mt={4} text='Save Costing' onPress={handleSubmit(submitCosting)} />
        </ScrollView>
      </Modal>
    </Box>
  );
};
Costing.propTypes = {
  onDrawerClick: PropTypes.func,
  route: PropTypes.object,
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: '#f4f4f4',
    height: '100%',
    marginTop: 1,
  },
  modal: {
    alignItems: 'center',
    backgroundColor: '#fff',
    height: '95%',
    width: '85%',
    borderRadius: 20,
  },
  imageStyle: {
    width: 160,
    height: 160,
    resizeMode: 'contain',
    margin: 6,
    backgroundColor: '#f7f8fa',
    borderRadius: 8,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20,
  },
});

export default Costing;
