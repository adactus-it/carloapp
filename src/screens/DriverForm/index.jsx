import React, { useState, useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import { Box, Text, ScrollView, useToast } from 'native-base';
import { SwitchComponent } from 'src/components/molecules';
import FirstForm from './FirstForm';
import { AppContext } from 'src/store';
import {
  ConfirmationDialog,
  CarIssues,
  CarPhotos,
} from 'src/components/organisms';
import { showApiErrorToast } from 'src/utils';
import {
  getDriverFormForOrder, getDriverFormPdf,
  updateDriverFormCarIssues,
  updateDriverFormForOrder,
  updateDriverFormMedia,
} from 'src/api/driver-forms';
import {
  answerQuestion,
  getQuestions,
  submitDriverSignature,
} from '../../api/driver-forms';
import { QuestionForm } from '../../components/organisms';
import { useTranslation } from 'react-i18next';
import { CustomerSignatureComponent } from '../../components/organisms/CustomerSignature';
import { CommonActions } from '@react-navigation/native';

function DriverForm({ navigation, route }) {
  const { t } = useTranslation();

  const headings = [
    t('pickup_info'),
    t('JFz-Ident.Nr agrees with vehicle registration'),
    t('Issues_in_car'),
    t('Car_Photos'),
    t(''),
  ];
  const toast = useToast();

  const stages = ['info', 'questions', 'car_issues', 'car_photos', 'signature'];

  const numberOfForms = 5;

  const [formStates, setFormStates] = useState({});
  const [activeForm, setActiveForm] = useState();
  const [currentFormId, setCurrentFormId] = useState(1);
  const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
  const [questions, setQuestions] = useState([]);
  const [currentQuestion, setCurrentQuestion] = useState({});
  const [pdfUrl, setPdfUrl] = useState('');

  useEffect(() => {
    (async () => {
      const pdfRes = await getDriverFormPdf(orderId);
      if (pdfRes && pdfRes.body) {
        const { pdf_url } = pdfRes.body;
        setPdfUrl(pdf_url);
        console.log({ pdf_url });
      }
    })();
  }, [orderId]);

  const appendToFormState = (obj) => {
    setFormStates({ ...formStates, ...obj });
  };
  const { forms, setLoading } = useContext(AppContext);

  const { orderId } = route?.params || { orderId: 3296 };

  async function fetchData(questions) {
    try {
      const res = await getDriverFormForOrder(orderId);
      console.log({ res });

      if (res?.body?.driver_form) {
        const { driver_form } = res.body;
        if (driver_form.active_stage?.includes('question')) {
          const currentQuestionId = driver_form.active_stage.split('_')[1];
          const currentQuestion = questions.find(
            ({ id }) => id === currentQuestionId,
          );
          setCurrentQuestion(currentQuestion);
          setActiveForm(2);
        } else {
          const stage = stages.indexOf(driver_form.active_stage);
          const setting = stage >= 0 ? stage + 1 : 1;
          setActiveForm(setting);
        }
        setFormStates(driver_form);
        setCurrentFormId(driver_form.id);
      }
    } catch (err) {
      showApiErrorToast(err, toast);
    }
  }

  async function fetchQuestions() {
    try {
      const res = await getQuestions();

      if (!res?.body) throw new Error('No response from server');
      const { questions } = res.body;

      if (questions && questions.length > 0) {
      }
      setQuestions(questions);
      return questions;
    } catch (e) {
      showApiErrorToast(e, toast);
    }
  }

  useEffect(() => {
    if (activeForm > numberOfForms) setActiveForm(1);
  }, [activeForm]);

  useEffect(() => {
    fetchQuestions().then(fetchData);
  }, []);

  const saveOrder = () => {
    forms.update(orderId, { formStates, activeForm });
  };

  async function onSubmit(data) {
    try {
      appendToFormState(data);
      // return;
      const response = await updateDriverFormForOrder(orderId, {
        ...formStates,
        ...data,
      });
      const {
        body: {
          driver_form: { id },
        },
      } = response;
      setCurrentFormId(id);

      saveOrder();

      moveToNext();
      setCurrentQuestion(questions[0]);
    } catch (error) {
      const message =
        error?.data?.message || error?.message || 'Connection error';
      message && toast.show({ title: message });
    }
  }

  function moveToNext() {
    if (activeForm < numberOfForms) {
      setActiveForm(activeForm + 1);
      return;
    }

    ConfirmationDialog('Proceed to the customer form?', () => {
      navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [
            {
              name: 'CustomerForm',
              params: { orderId },
            },
          ],
        }),
      );
    });
  }

  async function handleCarIssuesSubmit(car_issues) {
    if (car_issues.length < 1) {
      return moveToNext();
    }
    setLoading(true);
    try {
      await updateDriverFormCarIssues(currentFormId, car_issues);
      saveOrder();
      moveToNext();
    } catch (error) {
      const message =
        error?.data?.message || error?.message || 'Connection error';
      message && toast.show({ title: message });
    } finally {
      setLoading(false);
    }
  }

  async function handleCarMediaSubmit(photos) {
    try {
      setLoading(true);
      await updateDriverFormMedia(currentFormId, photos, 'driver_form');
      saveOrder();
      moveToNext();
    } catch (error) {
      showApiErrorToast(error, toast);
    } finally {
      setLoading(false);
    }
  }

  async function handleQuestionImages(photos) {
    try {
      setLoading(true);
      await updateDriverFormMedia(currentFormId, photos, 'driver_form');
    } catch (error) {
      showApiErrorToast(error, toast);
    } finally {
      setLoading(false);
    }
  }

  async function handleSignatureSubmit(signature) {
    //waiting for api's.
    try {
      setLoading(true);
      await submitDriverSignature(orderId, { ...formStates, signature });
      saveOrder();
      moveToNext();
    } catch (error) {
      showApiErrorToast(error, toast);
    } finally {
      setLoading(false);
    }
  }

  async function handleQuestionSubmit(option) {
    try {
      setCurrentQuestion(questions[currentQuestionIndex + 1]);
      setCurrentQuestionIndex(currentQuestionIndex + 1);
      setLoading(true);
      await answerQuestion(currentFormId, currentQuestion.id, option);
      saveOrder();
      if (currentQuestion.id < questions.length) {
        const nextIndex = parseInt(currentQuestion.id);
        const next = questions[nextIndex];
        if (next) {
          setCurrentQuestion(next);
          return;
        }
      }
      setActiveForm(3);
    } catch (error) {
      showApiErrorToast(error, toast);
    } finally {
      setLoading(false);
    }
  }

  return (
    <ScrollView
      contentContainerStyle={{
        minHeight: '100%',
      }}
    >
      <Box>
        <Text fontSize='2xl' style={{ textAlign: 'center', marginTop: 20 }}>
          {headings[activeForm - 1]}
        </Text>
      </Box>
      <Box>
        <SwitchComponent active={activeForm}>
          <FirstForm key={1} data={formStates} onSubmit={onSubmit} />
          <QuestionForm
            key={2}
            question={currentQuestion}
            onSubmit={handleQuestionSubmit}
            onPhotoSubmit={handleQuestionImages}
          />
          <CarIssues
            key={3}
            data={formStates}
            onSubmit={handleCarIssuesSubmit}
          />
          <CarPhotos
            key={4}
            data={formStates}
            onSubmit={handleCarMediaSubmit}
          />
          <CustomerSignatureComponent
            title='Sign here'
            key={5}
            data={formStates}
            onSubmit={handleSignatureSubmit}
            pdfUrl={pdfUrl}
          />
        </SwitchComponent>
      </Box>
    </ScrollView>
  );
}

DriverForm.propTypes =
  {
    t: PropTypes.func,
    navigation: PropTypes.object,
    route: PropTypes.object,
  };

export default withTranslation()(DriverForm);
