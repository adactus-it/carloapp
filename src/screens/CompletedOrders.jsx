/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useContext, useEffect, useState } from 'react';
import { FlatList, StyleSheet, View } from 'react-native';
import { OrderCard } from 'src/components/organisms';
import { HamburgerIcon, IconButton, Text, useToast } from 'native-base';
import PropTypes from 'prop-types';
import { AppContext } from 'src/store';
import { showApiErrorToast } from '../utils';
import { fetchCompletedOrders } from '../api/orders';
import { useTranslation } from 'react-i18next';

const CompletedOrders = ({ navigation }) => {
  const [completedOrders, setCompletedOrders] = useState([]);
  const { setLoading } = useContext(AppContext);
  const toast = useToast();
  const {t} = useTranslation();

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const { orders } = await fetchCompletedOrders();
        setCompletedOrders(orders);
      } catch (err) {
        showApiErrorToast(err, toast);
      } finally {
        setLoading(false);
      }
    };
    fetchData();
  }, []);
  useEffect(() => {
    const hamburgerIcon = () => (
      <IconButton
        icon={<HamburgerIcon color='#fff' />}
        transparent
        onPress={() => {
          navigation.openDrawer();
        }}
        style={{ color: '#fff' }}
      />
    );
    navigation.setOptions({
      headerLeft: hamburgerIcon,
    });
  }, [navigation]);

  return (
    <View>
      <View style={homeStyles.container}>
        <FlatList
          data={completedOrders}
          numColumns={1}
          ListFooterComponent={() => <View style={{ height: 250 }}></View>}
          showsVerticalScrollIndicator={false}
          ListEmptyComponent={
            <Text alignSelf='center' mt={10}>
              {t("nothing_to_show")}
            </Text>
          }
          renderItem={({ item, index }) => <OrderCard key={index} item={item} cardType={'completed'} />}
          keyExtractor={({item, index}) => index}
        />
      </View>
    </View>
  );
};

CompletedOrders.propTypes = {
  onDrawerClick: PropTypes.func,
  navigation: PropTypes.object,
};

const homeStyles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: '#f4f4f4',
    height: '100%',
  },
});

export default CompletedOrders;
