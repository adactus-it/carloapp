import React from 'react';
import { Button as B } from 'native-base';
import { StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

function Button({ text, style, disabled, icon, ...props }) {
  style = { ...styles?.button, ...style };

  return (
    <B
      iconLeft={icon != null}
      primary
      block
      style={disabled ? { ...style, ...styles.disabledButton } : style}
      {...props}
    >
      {icon || <></>}
      {text || ''}
    </B>
  );
}

const styles = StyleSheet.create({
  button: {
    color: '#fff',
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'center',
  },
  disabledButton: {
    backgroundColor: '#AAA',
  },
});

Button.propTypes = {
  onPress: PropTypes.func.isRequired,
  text: PropTypes.string,
  icon: PropTypes.string,
  style: PropTypes.object,
  disabled: PropTypes.bool,
};

export { Button };
