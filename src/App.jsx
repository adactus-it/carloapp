// eslint-disable-next-line no-unused-vars
import React from 'react';
import './lang/i18n';

import Router from './router';
import { LogBox, StatusBar } from 'react-native';
import { NativeBaseProvider } from 'native-base';
import { AppContextProvider } from './store';
import { NBTheme } from './theme';
import { LoadingOverlay } from './components/organisms/LoadingOverlay';


export default function App() {
  LogBox.ignoreLogs([
    'Remote',
    'Warning',
    'Warning:',
    'Accessing',
    'RCTBr',
    'Virtual',
    'Animated',
    'currentlyFocusedField',
  ]);


  return (
    <NativeBaseProvider theme={NBTheme}>
      <AppContextProvider>
        <StatusBar barStyle='light-content' backgroundColor='#23619C' />
        <LoadingOverlay/>
        <Router />
      </AppContextProvider>
    </NativeBaseProvider>
  );
}
