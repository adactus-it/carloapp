import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { HStack, VStack, Checkbox, Input } from 'native-base';
import { withTranslation } from 'react-i18next';
import { Button } from 'src/components/molecules';
import { useNavigation } from '@react-navigation/native';
import { technical_defects } from '../../utils';
import { LayoutAnimation, View } from 'react-native';

function InspectionDetail({ t, onSubmit, data, skipThisForm }) {
  const navigation = useNavigation();
  const [defects, setDefects] = useState(technical_defects);

  const handleChange = (value, object) => {
    const shallowCopy = JSON.parse(JSON.stringify(defects));
    const findObject = shallowCopy.find(item => item.id == object.id);
    findObject.value = value;
    setDefects(shallowCopy);
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
  };

  const handleNote = (value, object) => {
    const shallowCopy = JSON.parse(JSON.stringify(defects));
    const findObject = shallowCopy.find(item => item.id == object.id);
    findObject.note = value;
    setDefects(shallowCopy);
  };

  useEffect(() => {
    let formStates = data;
    if (!formStates?.visual_inspection_done) {
      if (formStates?.visual_inspection_done === '0' || !formStates?.visual_inspection_done) {
        skipThisForm();
      }
    }
  }, [data]);
  const parseValues = (values) => {
    const dto = {};
    const { checkboxes } = values;

    if (checkboxes && checkboxes.length > 1) for (const val of checkboxes) dto[val] = true;
    delete values.checkboxes;
    const sent = { ...values, ...dto };
    onSubmit(sent);
  };

  return (
    <VStack space={2} mx={4} alignItems='flex-start'>

      {
        defects.map((item) => {
          return (
            <View key={item?.id}>
              <Checkbox style={{ marginLeft: 0 }} ariaLabel={item?.id} value={item?.value}
                        onChange={(value) => handleChange(value, item)}>{item?.name}</Checkbox>
              {
                item?.value &&
                <Input
                  onChangeText={(event) => handleNote(event, item)}
                  placeholder={'Note'}
                />
              }
            </View>
          );
        })
      }
      <HStack justifyContent='space-around' w='100%' my={4}>
        <Button
          text={t('Save Order')}
          onPress={() => {
            navigation.navigate('HomeScreen');
          }}
        />
        <Button text={t('Continue')} onPress={() => onSubmit({ technical_defects: defects })} />
      </HStack>
    </VStack>
  );
}

InspectionDetail.propTypes = {
  t: PropTypes.func,
  navigation: PropTypes.object,
  onSubmit: PropTypes.func,
  data: PropTypes.object,
  skipThisForm: PropTypes.func,
};

export default withTranslation()(InspectionDetail);
