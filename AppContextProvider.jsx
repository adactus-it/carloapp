import React from 'react'
import PropTypes from 'prop-types'

import {}

function AppContextProvider({ children }) {}

AppContextProvider.propTypes = {
	children: PropTypes.node.isRequired,
}

export { AppContextProvider }
