import { makeRequest, toFormData } from 'src/utils';

export async function getDriverSchedule() {
  const response = await makeRequest('get', 'driver_schedule');
  console.log({ response });
  const {
    body: { driver_schedule },
  } = response;
  return { driver_schedule };
}

export function updateDriverSchedule(schedule) {
  return makeRequest('post', 'driver_schedule', toFormData(schedule));
}
