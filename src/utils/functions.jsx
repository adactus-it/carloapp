import { isDate, parse } from 'date-fns';

function parseDateString(value, originalValue) {
  return isDate(originalValue) ? originalValue : parse(`${originalValue}`, 'HH:mm', new Date());
}

function toFormData(obj) {
  const formData = new FormData();
  const objectKeys = Object.keys(obj);
  for (const x of objectKeys) {
    if (isDate(obj[x])) {
      formData.append(x, new Date(obj[x]).toISOString());
      continue;
    }
    formData.append(x, obj[x]);
  }
  return formData;
}

function imageToFormData(key, image) {
  const { path, mime } = image;
  const imageData = {
    uri: path,
    type: mime,
    name: `${Date.now()}.${getImageExtension(image)}`,
  };
  const formData = new FormData();
  formData.append(key, imageData);
  return formData;
}

function multipleImageToFormData(key, images) {
  const formData = new FormData();
  images.forEach(element => {
    const { path, mime } = element;
    const imageData = {
      uri: path,
      type: mime,
      name: `${Date.now()}.${getImageExtension(element)}`,
    };
    formData.append(key, imageData);
  });

  return formData;
}


function showApiErrorToast(error, toast) {
  const message = error?.data?.message || error?.message || 'Connection error';
  message && toast.show({ title: message });
}

function getImageExtension(image) {
  const arr = image?.path.split('.');
  if (arr.length === 1)
    return '';

  return arr.reverse()[0] || '';
}

function ucFirst(str = '') {
  return str.substr(0, 1).toUpperCase() + str.substr(1);
}

export { toFormData, parseDateString, showApiErrorToast, imageToFormData, multipleImageToFormData, ucFirst };
