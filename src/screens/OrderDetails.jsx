import React, { useLayoutEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import Swiper from 'react-native-swiper';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import { withTranslation } from 'react-i18next';
import { Pressable, VStack, Box, Text } from 'native-base';
import { startOrder } from '../api/orders';

export const OrderDetails = ({ navigation, route }) => {
  const { order } = route.params;

  const _startIcon = () => (
    <Pressable
      style={{ marginRight: 10, alignItems: 'center' }}
      onPress={async () => {
        const orderResponse = await startOrder(order.id);
        if (orderResponse?.body?.form_stage === "car_return_form") {
          navigation.navigate('CarReturn', { orderId: order.id });
        } else {
          navigation.navigate('DriverForm', { orderId: order.id });
        }
      }}
    >
      <EntypoIcon name={'controller-play'} size={24} color={'#FFF'} />
      <Text style={{ color: '#FFF' }}>Start</Text>
    </Pressable>
  );

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: _startIcon,
    });
  }, [navigation]);

  return (
    <Box>
      <Box style={{ display: 'flex', height: '100%', flexDirection: 'column' }}>
        <Box
          style={{
            padding: 12,
            alignItems: 'center',
            borderWidth: 1,
            borderLeftWidth: 0,
            borderRightWidth: 0,
            borderColor: '#0b5ea2',
          }}
        >
          <Text style={{ fontSize: 20, color: '#0b5ea2' }}>Carlogistics Auftrags-Nr: {order.order_number}</Text>
        </Box>
        <Box style={{ flex: 1, backgroundColor: '#fafafb' }}>
          <Swiper
            // from={1}
            style={styles.wrapper}
            showsButtons={false}
          >
            <Box style={[styles.slide, { display: 'flex', justifyContent: 'center' }]}>
              <Box style={styles.slideStyle}>
                <Text underline bold fontSize='lg' alignSelf='center' buttonStyle={styles.buttonStyle}>
                  Remarks
                </Text>
                <Box style={{ marginTop: 20 }}>
                  <Text style={styles.bodyText}>{order.remarks || 'No remarks'}</Text>
                </Box>
              </Box>
            </Box>
            <Box style={[styles.slide, { display: 'flex', justifyContent: 'center' }]}>
              <Box style={styles.slideStyle}>
                <Text fontSize='lg' underline bold alignSelf='center'>
                  Pickup Information
                </Text>
                <VStack space={2} style={{ padding: 10 }}>
                  <Box style={styles.infoSection}>
                    <Text fontSize='md'>Date</Text>
                    <Text color='#777'>{order.date_of_pick_up}</Text>
                  </Box>
                  <Box style={[styles.infoSection]}>
                    <Text fontSize='md' style={{ alignSelf: 'flex-start' }}>
                      Time
                    </Text>
                    <Text color='#777'>{order.pick_up_time}</Text>
                  </Box>

                  <Box style={styles.infoSection}>
                    <Text fontSize='md'>Pickup Contact</Text>
                    <Text color='#777'>{order.pick_up_contact_person}</Text>
                  </Box>
                  <Box style={styles.infoSection}>
                    <Text fontSize='md'>Phone Number</Text>
                    <Text color='#777'>{order.pick_up_contact_person_phone_no}</Text>
                  </Box>
                  {
                    order?.pick_up_alternative_contact_person
                      ?
                      <Box style={styles.infoSection}>
                        <Text fontSize='md'>Alternate Pickup Contact</Text>
                        <Text color='#777'>{order.pick_up_alternative_contact_person}</Text>
                      </Box>
                      :
                      <></>
                  }
                  {
                    order?.pick_up_alternative_contact_person_phone_no
                      ?
                      <Box style={styles.infoSection}>
                        <Text fontSize='md'>Alternate Phone Number</Text>
                        <Text color='#777'>{order.pick_up_alternative_contact_person_phone_no}</Text>
                      </Box>
                      :
                      <></>
                  }
                  <Box>
                    <Box style={[styles.infoSection, { width: '100%' }]}>
                      <Text fontSize='md'>Address</Text>
                      <Text>
                        {`${order.pick_up_company_street} ${order.pick_up_company_city}, ${order.pick_up_company_postcode}`}
                      </Text>
                    </Box>
                  </Box>
                </VStack>
              </Box>
            </Box>
            <Box style={[styles.slide, { display: 'flex', justifyContent: 'center' }]}>
              <Box style={styles.slideStyle}>
                <Text alignSelf='center' fontSize='lg' underline bold>
                  Delivery Information
                </Text>
                <VStack space={2} style={{ padding: 10 }}>
                  <Box style={styles.infoSection}>
                    <Text fontSize='md'>Date</Text>
                    <Text color='#777'>{order.date_of_delivery}</Text>
                  </Box>
                  <Box style={[styles.infoSection]}>
                    <Text fontSize='md'>Time</Text>
                    <Text color='#777'>{order.time_of_delivery}</Text>
                  </Box>

                  <Box style={styles.infoSection}>
                    <Text fontSize='md'>Pickup Contact</Text>
                    <Text color='#777'>{order.delivery_contact_person}</Text>
                  </Box>
                  <Box style={styles.infoSection}>
                    <Text fontSize='md'>Phone Number</Text>
                    <Text color='#777'>{order.delivery_contact_person_phone || '-'}</Text>
                  </Box>
                  <Box style={styles.infoSection}>
                    <Text fontSize='md'>Address</Text>
                    <Text color='#777'>
                      {`${order.delivery_road} ${order.delivery_place}, ${order.delivery_postal_code}`}
                    </Text>
                  </Box>
                </VStack>
              </Box>
            </Box>
            <Box style={[styles.slide, { display: 'flex', justifyContent: 'center' }]}>
              <Box style={styles.slideStyle}>
                <Text alignSelf='center' fontSize='lg' underline bold>
                  Vehicle Details
                </Text>
                <VStack space={2} style={{ padding: 10 }}>
                  <Box style={styles.infoSection}>
                    <Text fontSize='md'>Vehicle type</Text>
                    <Text color='#777'>{order.vehicle_type}</Text>
                  </Box>
                  <Box style={[styles.infoSection]}>
                    <Text fontSize='md'>Manufacturer</Text>
                    <Text color='#777'>{order.manufacturer}</Text>
                  </Box>
                  <Box style={styles.infoSection}>
                    <Text fontSize='md'>Model</Text>
                    <Text color='#777'>{order.model}</Text>
                  </Box>
                  <Box style={styles.infoSection}>
                    <Text fontSize='md'>VIN</Text>
                    <Text color='#777'>{order.VIN}</Text>
                  </Box>
                  <Box style={[styles.infoSection, { width: '100%' }]}>
                    <Text fontSize='md'>Red License Plate</Text>
                    <Text color='#777'>{order.red_license_plate}</Text>
                  </Box>
                </VStack>
              </Box>
            </Box>
            <Box style={[styles.slide, { display: 'flex', justifyContent: 'center' }]}>
              <Box style={styles.slideStyle}>
                <Text fontSize='lg' underline bold alignSelf='center'>
                  Car Returns
                </Text>
                <VStack space={2} style={{ padding: 10 }}>
                  <Box style={styles.infoSection}>
                    <Text fontSize='md'>Is There Return</Text>
                    <Text color='#777'>{order.return_firm_name}</Text>
                  </Box>
                  <Box style={styles.infoSection}>
                    <Text fontSize='md'>Address</Text>
                    <Text color='#777'>
                      {`${order.return_firm_street} ${order.return_firm_city}, ${order.return_firm_postal_code}`}
                    </Text>
                  </Box>
                  <Box style={styles.infoSection}>
                    <Text fontSize='md'>Comment Returns</Text>
                    <Text color='#777'>{order.return_remark}</Text>
                  </Box>
                </VStack>
              </Box>
            </Box>
          </Swiper>
        </Box>
      </Box>
    </Box>
  );
};

OrderDetails.propTypes = {
  navigation: PropTypes.object,
  route: { params: { order: PropTypes.object } },
};

const styles = StyleSheet.create({
  wrapper: {},
  slide: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  buttonStyle: {
    backgroundColor: '#0b5ea2',
    width: '100%',
    height: 45,
    color: '#fff',
  },
  bodyText: {
    color: '#0b5ea2',
    fontSize: 16,
  },
  slideStyle: {
    height: '95%',
    padding: 6,
    width: '100%',
    borderRadius: 12,
    backgroundColor: '#fff',
    shadowOffset: { width: 0.95, height: 0.95 },
    shadowColor: '#a2a2a2',
    shadowOpacity: 0.5,
    shadowRadius: 2,
  },
  infoContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    marginTop: 10,
  },
  infoSection: {
    display: 'flex',
    flexDirection: 'column',
    width: 200,
  },
});

export default withTranslation()(OrderDetails);
