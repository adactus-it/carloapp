import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import ImagePicker from 'react-native-image-crop-picker';
import { CloseIcon, Flex, Box, Text, Image, Modal, ScrollView, SimpleGrid, Pressable } from 'native-base';
import { Button } from 'src/components/molecules';
import { useNavigation } from '@react-navigation/native';
import { withTranslation } from 'react-i18next';
import { ConfirmationDialog } from './ConfirmationDialog';
import { StyleSheet, View } from 'react-native';
import { vh } from '../../theme';
import cameraAngle from '../../assets/cameraAngle.png'
function CarPhotos({ onSubmit, t }) {
  const navigation = useNavigation();
  const [photos, setPhotos] = React.useState([]);
  const [showModal, setShowModal] = React.useState(false);
  const [selectedImage, setSelectedImage] = React.useState('');

  useEffect(() => {
    return () => {
      ImagePicker.clean()
        .then(() => {
        })
        .catch((e) => {
          alert(e);
        });
    };
  }, []);

  const multiImagePicker = () =>
    ImagePicker.openPicker({
      multiple: true,
      compressImageQuality: 0.8
    }).then((images) => {
      setPhotos([...photos, ...images.map(({ path, filename, mime }) => ({
        path,
        mime,
      }))]);
    });

  const openCamera = () =>
    ImagePicker.openCamera({
      cropping: true,
      compressImageQuality: 0.8
    }).then(({ path, mime }) => {
      setPhotos([...photos, {
        path,
        mime,
      }]);
    });

  const removeImage = (removedImage) => {
    setPhotos(photos.filter((it) => it.path !== removedImage.path));
  };

  const openImage = (image) => {
    setSelectedImage(image);
    setShowModal(true);
  };

  const handleSubmit = () => onSubmit(photos);

  return (
    <ScrollView>
      <Box w='100%' align='center' direction='column' justify='center'>
        <Button
          text='Take Car photos'
          m={4}
          onPress={() => {
            ConfirmationDialog(
              'Select source of photos',
              openCamera,
              'Select Car photos',
              'Camera',
              'Gallery',
              multiImagePicker
            );
          }}
        />
        {photos.length > 0 ? (
          <SimpleGrid flex={1} columns={3} space={4} mx={4}>
            {photos.map((photo, index) => {
              return (
                <Box key={photo.uri} w={100} h={100} position='relative'>
                  <Image
                    source={{ uri: photo.path }}
                    h={100}
                    w={100}
                    resizeMode='cover'
                    alt={`Photo: ${index + 1}`}
                    onPress={() => openImage(photo)}
                  />
                  <Pressable
                    position='absolute'
                    top={-8}
                    right={-5}
                    elevation={2}
                    bg='white'
                    p={1}
                    borderRadius={1000}
                    onPress={() => {
                      removeImage(photo);
                    }}
                  >
                    <CloseIcon w={2} h={2} />
                  </Pressable>
                </Box>
              );
            })}
          </SimpleGrid>
        ) : (
          <Text alignSelf='center' mb={4}>
            No photos to display
          </Text>
        )}

        <Flex mt={4} direction='row' justify='space-around'>
          <Button
            text={t('Save Order')}
            onPress={() => {
              navigation.navigate('HomeScreen');
            }}
          />
          {
            photos.length >= 6 && <Button text={t('Continue')} onPress={handleSubmit} />
          }
        </Flex>
        <Text alignSelf='center' textAlign={"center"} mt={5} mb={5}>
          {t("photos_caution")}
        </Text>
        <View style={styles.pictureContainer}>
          <Image alt='cameraAngle' style={styles.cameraAngle} source={cameraAngle} />
        </View>
        <Modal
          transparent={false}
          animationType={'fade'}
          visible={showModal}
          onRequestClose={() => {
            setShowModal(false);
          }}
        >
          <Box>
            <Image
              mb={4}
              flex={1}
              resizeMode='contain'
              w={150}
              h={150}
              source={{ uri: selectedImage }}
              alt={'Selected'}
            />
            <Box
              activeOpacity={0.5}
              onPress={() => {
                setShowModal(false);
              }}
            >
              <Image
                source={{
                  uri: 'https://raw.githubusercontent.com/AboutReact/sampleresource/master/close.png',
                }}
                style={{ width: 35, height: 35 }}
              />
            </Box>
          </Box>
        </Modal>
      </Box>
    </ScrollView>
  );
}

CarPhotos.propTypes = {
  onSubmit: PropTypes.func,
  t: PropTypes.func,
};

export const CarPhotosComponent = withTranslation()(CarPhotos);


const styles = StyleSheet.create({
  pictureContainer: {
    height: vh * 30,
    width: "90%",
    alignSelf: "center",
  },
  cameraAngle: {
    width: "100%",
    height: "100%",
    resizeMode: "contain"
  }
})