import { request, toFormData, makeRequest, imageToFormData, multipartHeaders } from 'src/utils';

const getCollectionUrl = (orderId) => `orders/${orderId}/costing`;

function endpoint(method, orderId, data) {
  return makeRequest(method, getCollectionUrl(orderId), data && toFormData(data), {
    headers: {
      'Content-Type': 'application/json',
    },
  });

}

export function getCostingForOrder(orderId) {
  return endpoint('get', orderId);
}

export function createCostingForOrder(orderId, data) {
  return endpoint('post', orderId, data);
}

export function updateCostingForOrder(costingId, data) {
  return makeRequest('patch', `costing/${costingId}`, data, multipartHeaders);
}

export function deleteCostingForOrder(costingId) {
  return makeRequest('delete', `costing/${costingId}`);
}

