/* eslint-disable react/prop-types */
import React, { useContext } from 'react';
import { Image, StyleSheet, View, Text } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { DrawerItem, createDrawerNavigator, DrawerContentScrollView } from '@react-navigation/drawer';
import Animated from 'react-native-reanimated';
import AppHome from './AppHome';
import CompletedOrders from './CompletedOrders';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import LinearGradient from 'react-native-linear-gradient';
import { AppContext } from 'src/store';
import { ConfirmationDialog } from 'src/components/organisms';
import { useTranslation } from 'react-i18next';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const Screens = ({ style }) => {
  return (
    <Animated.View style={StyleSheet.flatten([styles.stack, style])}>
      <Stack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: '#23619C',
          },
          headerTintColor: '#FFF',
        }}
      >
        <Stack.Screen options={{ headerTitle: 'Orders' }} name='AppHome'>
          {(props) => <AppHome {...props} />}
        </Stack.Screen>
        <Stack.Screen name='CompletedOrders'>{(props) => <CompletedOrders {...props} />}</Stack.Screen>
      </Stack.Navigator>
    </Animated.View>
  );
};

const DrawerContent = (props) => {
const {t} = useTranslation();
  const { logout, auth } = useContext(AppContext);
  return (
    <DrawerContentScrollView {...props} scrollEnabled={false} contentContainerStyle={{ flex: 1 }}>
      <View style={{ height: '100%' }}>
        <View style={{ display: 'flex', flexDirection: 'row', margin: 20, alignItems: 'center' }}>
          <Image
            source={{
              uri: 'https://reactjsexample.com/assets/favicon.png',
              height: 60,
              width: 60,
              scale: 0.5,
            }}
            resizeMode={'cover'}
            style={[styles.avatar, { backgroundColor: 'black' }]}
          />
          <View style={{ display: 'flex', flexDirection: 'column', marginBottom: 20, marginLeft: 10 }}>
            <Text style={styles.userName}>{auth && auth.name}</Text>
            <Text style={styles.activityStatus}>Online</Text>
          </View>
        </View>
        <View>
          <DrawerItem
            label= {t('homescreen')}
            labelStyle={styles.drawerLabel}
            style={styles.drawerItem}
            onPress={() => props.navigation.navigate('AppHome')}
            icon={() => <MaterialIcons name='dashboard' color='white' size={26} />}
          />
          <DrawerItem
            label={t('driver_schedule')}
            labelStyle={styles.drawerLabel}
            style={styles.drawerItem}
            onPress={() => props.navigation.navigate('DriverSchedule')}
            icon={() => <MaterialIcons name='dashboard' color='white' size={26} />}
          />
          <DrawerItem
            label={t('completed_orders')}
            labelStyle={styles.drawerLabel}
            style={styles.drawerItem}
            onPress={() => props.navigation.navigate('CompletedOrders')}
            icon={() => <Feather name='list' color='white' size={26} />}
          />
          <DrawerItem
            label={t('Logout')}
            labelStyle={styles.drawerLabel}
            style={styles.drawerItem}
            onPress={() => {
              ConfirmationDialog(t("logout_confirmation"), () => {
                logout();
              });
            }}
            icon={() => <Feather name='log-out' color='white' size={26} />}
          />
        </View>
      </View>
    </DrawerContentScrollView>
  );
};

export default () => {
  const [progress, setProgress] = React.useState(new Animated.Value(0));
  const scale = Animated.interpolate(progress, {
    inputRange: [0, 1],
    outputRange: [1, 0.8],
  });
  const borderRadius = Animated.interpolate(progress, {
    inputRange: [0, 1],
    outputRange: [0, 16],
  });

  const animatedStyle = { borderRadius, transform: [{ scale }] };

  return (
    <LinearGradient style={{ flex: 1 }} colors={['#1c8cc1', '#0c62a5']}>
      <Drawer.Navigator
        drawerType='slide'
        overlayColor='transparent'
        drawerStyle={styles.drawerStyles}
        contentContainerStyle={{ flex: 1 }}
        drawerContentOptions={{
          activeBackgroundColor: 'transparent',
          activeTintColor: 'white',
          inactiveTintColor: 'white',
        }}
        sceneContainerStyle={{ backgroundColor: 'transparent' }}
        drawerContent={(props) => {
          setProgress(props.progress);
          return <DrawerContent {...props} />;
        }}
      >
        <Drawer.Screen name='Screens'>{(props) => <Screens {...props} style={animatedStyle} />}</Drawer.Screen>
      </Drawer.Navigator>
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  stack: {
    flex: 1,
    shadowColor: '#FFF',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 5,
    // overflow: 'scroll',
    // borderWidth: 1,
  },
  drawerStyles: { flex: 1, width: '50%', backgroundColor: 'transparent' },
  drawerItem: { alignItems: 'flex-start', marginVertical: 0 },
  drawerLabel: { color: 'white', marginLeft: -30, width: 150, fontSize: 14 },
  avatar: {
    borderRadius: 60,
    marginBottom: 16,
    borderColor: 'white',
    borderWidth: StyleSheet.hairlineWidth,
  },
  userName: { color: '#fff', fontSize: 16, fontWeight: '600' },
  activityStatus: { color: '#fff', fontSize: 14 },
});
