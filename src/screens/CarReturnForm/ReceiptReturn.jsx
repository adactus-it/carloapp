import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';

import { Text, HStack, VStack, Checkbox, Radio } from 'native-base';
import { withTranslation } from 'react-i18next';
import { Button, FormControl } from 'src/components/molecules';
import { useNavigation } from '@react-navigation/native';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

const FUEL_GAUGES = ['1/1', '3/4', '1/2', '1/4', 'Reserve'];

function ReceiptReturn({ t, onSubmit }) {
  const navigation = useNavigation();

  const schema = yup.object().shape({
    receipt_mileage: yup.number().required().typeError('Not a valid number'),
    reciept_license_plate: yup.bool().required(),
    reciept_fuel_gauge: yup.string().required(),
    reciept_keys: yup.bool().required(),
  });


  const {
    control,
    formState: { errors },
    getValues,
    handleSubmit,
  } = useForm({
    resolver: yupResolver(schema),
    defaultValues: {
      receipt_mileage: 0,
      receipt_license_plate: null,
      receipt_fuel_gauge: null,
      receipt_keys: null,
    },
  });

  const testSubmit = () => {
    const values = getValues();
    onSubmit(values);
    // handleSubmit(onSubmit)
  };

  return (
    <VStack space={2} mx={4}>

      <FormControl
        block
        control={control}
        isRequired={true}
        error={errors.mileage}
        name='receipt_mileage'
        keyboardType='numeric'
        textContentType='number'
        label='Mileage at handover'
      />
      <Text alignSelf='flex-start'>Motor Vehicle registration</Text>
      <FormControl block control={control} name={'receipt_licence'}>
        {({ field: { onChange, value } }) => (
          <Radio.Group
            accessibilityLabel='test'
            // justifyContent='space-around'
            flexDirection='row'
            flexWrap='wrap'
            name='Motor Vehicle Registration license'
            onChange={onChange}
            value={value}
          >
            <Radio accessibilityLabel='test' value={false} mx={1} my={0.5}>
              Nien
            </Radio>
            <Radio accessibilityLabel='test' value={true} mx={1} my={0.5}>
              Ja
            </Radio>
          </Radio.Group>
        )}
      </FormControl>

      <Text alignSelf='flex-start'>Fuel Gauge</Text>
      <FormControl block control={control} name={'receipt_fuel_gauge'} error={errors?.fuel_gauge}>
        {({ field: { onChange, value } }) => (
          <Radio.Group
            accessibilityLabel='test'
            justifyContent='space-around'
            flexDirection='row'
            flexWrap='wrap'
            name='fuel gauges'
            onChange={onChange}
            value={value}
          >
            {FUEL_GAUGES.map((it) => (
              <Radio accessibilityLabel='test' key={it} value={it} mx={1} my={0.5}>
                {it}
              </Radio>
            ))}
          </Radio.Group>
        )}
      </FormControl>

      <Text alignSelf='flex-start'>Keys</Text>
      <FormControl block control={control} name={'receipt_keys'}>
        {({ field: { onChange, value } }) => (
          <Radio.Group
            accessibilityLabel='test'
            // justifyContent='space-around'
            flexDirection='row'
            flexWrap='wrap'
            name='receipt_keys'
            onChange={onChange}
            value={value}
          >
            <Radio accessibilityLabel='test' value={false} mx={1} my={0.5}>
              Nien
            </Radio>
            <Radio accessibilityLabel='test' value={true} mx={1} my={0.5}>
              Ja
            </Radio>
          </Radio.Group>
        )}
      </FormControl>
      <HStack justifyContent='space-around' w='100%' my={4}>
        <Button
          text={t('Save Order')}
          onPress={() => {
            navigation.navigate('HomeScreen');
          }}
        />
        <Button text={t('Continue')} onPress={testSubmit} />
      </HStack>
    </VStack>
  );
}

ReceiptReturn.propTypes = {
  t: PropTypes.func,
  navigation: PropTypes.object,
  onSubmit: PropTypes.func,
};

export default withTranslation()(ReceiptReturn);
