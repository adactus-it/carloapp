import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { Box, HStack, Text } from 'native-base';
import { Button } from 'src/components/molecules';
import Signature from 'react-native-signature-canvas';
import { useNavigation } from '@react-navigation/native';
import { withTranslation } from 'react-i18next';
import InfoContainer from '../molecules/InfoContainer';

function CustomerSignature({ title = 'Take Customer Signature', onSubmit, t, pdfUrl }) {
  const [sign, setSign] = useState(null);
  const navigation = useNavigation();

  useEffect(() => {
    if (pdfUrl) {
      navigation?.setOptions({
        headerRight: () => InfoContainer(pdfUrl),
      });
    }
  }, [pdfUrl]);

  return (
    <Box style={styles.signatureContainer}>
      <Text style={[styles.headingText, { marginLeft: 20, backgroundColor: 'transparent' }]}>{title}</Text>
      <Box style={styles.signatureStyle}>
        <Signature
          dataURL={sign}
          onOK={setSign}
          descriptionText='Sign'
          clearText='Clear'
          confirmText='Save'
          webStyle={sigStyle}
        />
      </Box>
      <HStack w='100%' mt={4} justifyContent='space-around'>
        <Button
          text={t('Save Order')}
          onPress={() => {
            navigation.navigate('HomeScreen');
          }}
        />
        <Button
          disabled={!sign}
          text={t('Continue')}
          onPress={() => {
            console.log({ sign });
            onSubmit(sign);
          }}
        />
      </HStack>
    </Box>
  );
}

CustomerSignature.propTypes = {
  onSubmit: PropTypes.func,
  t: PropTypes.func,
  title: PropTypes.string,
  pdfUrl: PropTypes.string,
};

const styles = StyleSheet.create({
  headingText: {
    color: '#0b5ea2',
    fontSize: 16,
    fontWeight: '500',
  },
  signatureContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',

    height: '90%',
    width: '100%',
  },
  signatureStyle: {
    borderRadius: 12,
    marginTop: 20,
    shadowOffset: { width: 0.95, height: 0.95 },
    shadowColor: '#a2a2a2',
    shadowOpacity: 0.8,
    shadowRadius: 4,
    backgroundColor: '#fff',
    height: 400,
    width: '80%',
  },
});

const sigStyle = StyleSheet.create({
  // eslint-disable-next-line react-native/no-unused-styles
  preview: {
    width: 335,
    height: 214,
    backgroundColor: '#F8F8F8',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 15,
  },
  // eslint-disable-next-line react-native/no-unused-styles
  previewText: {
    color: '#FFF',
    fontSize: 14,
    height: 40,
    lineHeight: 40,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#69B2FF',
    width: 120,
    textAlign: 'center',
    marginTop: 10,
  },
});

export const CustomerSignatureComponent = withTranslation()(CustomerSignature);
