import React from 'react';
import PropTypes from 'prop-types';
import { Box, Image } from 'native-base';
import Logo from 'src/assets/logo.png';
function SplashScreen() {
  return (
    <Box h='100%' w='100%' alignItems='center' justifyContent='center'>
      <Image source={Logo} resizeMode='contain' alt='CarLogistics' />
    </Box>
  );
}

SplashScreen.propTypes = {
  navigation: PropTypes.object,
};

export default SplashScreen;
