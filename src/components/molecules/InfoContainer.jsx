import { Image, Linking, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import React from 'react';
import { vh, vw } from '../../theme';
import { InfoIcon } from 'native-base';

const InfoContainer = (url) => {
  const onPdfView = () => {
    Linking.openURL(url);
  };
  return (
    <TouchableOpacity onPress={onPdfView} style={styles.container}>
      <InfoIcon name='list' color='white' size={26} />
    </TouchableOpacity>
  );
};

export default InfoContainer;

const styles = StyleSheet.create({
  container: {
    width: vh * 5,
    height: vh * 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: vw * 5,
  },
});
