import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { LayoutAnimation, StyleSheet, UIManager, View } from 'react-native';
import { Text, HStack, VStack, Radio, Checkbox } from 'native-base';
import { withTranslation } from 'react-i18next';
import { Button, FormControl } from 'src/components/molecules';
import { useNavigation } from '@react-navigation/native';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { parseDateString } from '../../utils';

const FUEL_GAUGES = ['1/1', '3/4', '1/2', '1/4', 'Reserve'];
if (
  Platform.OS === 'android' &&
  UIManager.setLayoutAnimationEnabledExperimental
) {
  UIManager.setLayoutAnimationEnabledExperimental(true);
}
const schema = yup.object().shape({
  start_time: yup
    .date()
    .transform(parseDateString)
    // .min(new Date(), `Start time must be later than ${format(new Date(), 'HH:mm')}`)
    .required(),
  end_time: yup
    .date()
    .transform(parseDateString)
    // .min(new Date(), `End time must be later than ${format(new Date(), 'HH:mm')}`)
    .required(),
  number_key: yup.string().required(),
  mileage: yup.number().required().typeError('Not a valid number'),
  fuel_gauge: yup.string().required(),
});

function CarDetails({ t, onSubmit, data }) {
  const navigation = useNavigation();
  const [briefing, setBriefing] = useState(false);
  const {
    control,
    formState: { errors },
    handleSubmit,
  } = useForm({
    resolver: yupResolver(schema),
    defaultValues: {
      start_time: data?.start_time || new Date(),
      end_time: data?.end_time || new Date(),
      mileage: data.mileage,
      briefing: false,
      briefing_time: '',
      motor_vehicle_registration: 0,
      service_booklet: 0,
      warning_triangle: 0,
      vest: 0,
      first_aid: 0,
      vehicle_clean: 0,
      extra_tires: 0,
    },
  });

  const parseValues = (values) => {
    const dto = {};
    const { checkboxes } = values;

    if (checkboxes && checkboxes.length > 1) for (const val of checkboxes) dto[val] = true;
    delete values.checkboxes;
    onSubmit({ ...values, ...dto });
  };

  const handleBriefingCheck = (value) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    setBriefing(value);
  };

  return (
    <VStack space={2} mx={4}>
      <Text my={5} style={styles.headingText}>
        {t('WAITING TIME')}
      </Text>
      <HStack space={2} align='center' justifyContent='space-between'>
        <FormControl
          error={errors?.start_time}
          name='start_time'
          control={control}
          label='From'
          placeholder='Select start time'
          type='date'
        />
        <FormControl
          error={errors?.end_time}
          name='end_time'
          control={control}
          label='To'
          placeholder='Select end time'
          type='date'
        />
      </HStack>


      <FormControl
        name='number_key'
        error={errors?.number_key}
        control={control}
        block
        textContentType='number'
        keyboardType='numeric'
        label='Number Key'
      />

      <FormControl
        block
        returnKeyType={'done'}
        control={control}
        isRequired={true}
        name='mileage'
        keyboardType='numeric'
        error={errors?.mileage}
        textContentType='number'
        label='Mileage at handover'
      />

      <FormControl block control={control} name='briefing'>
        {({ field: { onChange } }) => (
          <View style={styles.briefing}>
            <Checkbox onChange={(value => {
              onChange(value);
              handleBriefingCheck(value);
            })} my={0.5} value={'briefing'} accessibilityLabel='test'>
              Briefing
            </Checkbox>
          </View>
        )}
      </FormControl>
      {
        briefing &&
        <FormControl
          name='briefing_time'
          error={errors?.number_key}
          control={control}
          block
          textContentType='number'
          keyboardType='numeric'
          label={`${t('briefing_time')}`}
        />
      }

      <Text alignSelf='center'>Fuel Gauge</Text>
      <FormControl block control={control} name={'fuel_gauge'} error={errors?.fuel_gauge}>
        {({ field: { onChange, value } }) => (
          <Radio.Group
            accessibilityLabel='test'
            justifyContent='space-around'
            flexDirection='row'
            flexWrap='wrap'
            name='fuel gauges'
            onChange={onChange}
            value={value}
          >
            {FUEL_GAUGES.map((it) => (
              <Radio accessibilityLabel='test' key={it} value={it} mx={1} my={0.5}>
                {it}
              </Radio>
            ))}
          </Radio.Group>
        )}
      </FormControl>
      <FormControl block control={control} name='checkboxes'>
        {({ field: { onChange } }) => (
          <Checkbox.Group accessibilityLabel='test' onChange={onChange}>
            <Checkbox my={0.5} value={'motor_vehicle_registration'} accessibilityLabel='test'>
              Motor vehicle registration
            </Checkbox>
            <Checkbox my={0.5} value={'service_booklet'} accessibilityLabel='test'>
              Service booklet
            </Checkbox>
            <Checkbox my={0.5} value={'warning_triangle'} accessibilityLabel='test'>
              Warning triangle
            </Checkbox>
            <Checkbox my={0.5} value={'vest'} accessibilityLabel='test'>
              vest
            </Checkbox>
            <Checkbox my={0.5} value={'first_aid'} accessibilityLabel='test'>
              first aid
            </Checkbox>
            <Checkbox my={0.5} value={'vehicle_declaration'} accessibilityLabel='test'>
              Vehicle declaration
            </Checkbox>
            <Checkbox my={0.5} value={'vehicle_clean'} accessibilityLabel='test'>
              Vehicle inside and outside clean
            </Checkbox>
            <Checkbox my={0.5} value={'extra_tires'} accessibilityLabel='test'>
              2nd set of tires
            </Checkbox>
          </Checkbox.Group>
        )}
      </FormControl>

      <FormControl
        control={control}
        name='remarks'
        label='Remarks'
        placeholder='Enter remarks here'
        type='textarea'
        h={20}
        block
      />
      <HStack justifyContent='space-around' w='100%' my={4}>
        <Button
          text={t('Save Order')}
          onPress={() => {
            navigation.navigate('HomeScreen');
          }}
        />
        <Button text={t('Continue')} onPress={handleSubmit(parseValues)} />
      </HStack>
    </VStack>
  );
}

const styles = StyleSheet.create({
  headingText: {
    color: '#0b5ea2',
    textAlign: 'center',
  },
  briefing: {
    alignSelf: 'flex-start',
  },
});

CarDetails.propTypes = {
  t: PropTypes.func,
  navigation: PropTypes.object,
  onSubmit: PropTypes.func,
  data: PropTypes.object,
};

export default withTranslation()(CarDetails);
