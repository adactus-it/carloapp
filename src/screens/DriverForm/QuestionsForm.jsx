import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Box, useToast } from 'native-base';
import { withTranslation } from 'react-i18next';
import { getQuestions } from '../../api/driver-forms';
import { showApiErrorToast } from '../../utils';
import { QuestionForm } from '../../components/organisms';

function QuestionsForm({ onSubmit }) {
  const [questions, setQuestions] = useState([]);
  const [currentQuestionId, setCurrentQuestionId] = useState();

  const toast = useToast();

  // function submit() {
  //   onSubmit({ questions });
  // }

  function handleQuestionSubmit(question) {
  }
  return (
    <Box bg='white'>
      {questions.map(({ id, question }) => (
        <QuestionForm key={id} question={question} onSubmit={handleQuestionSubmit} />
      ))}
    </Box>
  );
}

QuestionsForm.propTypes = {
  onSubmit: PropTypes.func,
};

export default withTranslation()(QuestionsForm);
