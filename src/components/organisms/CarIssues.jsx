import React, { useEffect, useRef, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { Box, HStack, Pressable, Image, Checkbox, VStack, Text } from 'native-base';
import { withTranslation } from 'react-i18next';
import { Button } from 'src/components/molecules';
import Modal from 'react-native-modalbox';
import { useNavigation } from '@react-navigation/native';
import { PropTypes } from 'prop-types';
import CarCanvas from '../molecules/CarCanvas';

const ALL_ISSUES = [
  { code: 'dmg_K', name: 'K = Kratzer', state: true },
  { code: 'dmg_D', name: 'D = Delle/Beule', state: true },
  { code: 'dmg_R', name: 'R = Rost', state: true },
  { code: 'dmg_B', name: 'B = Blechschaden', state: true },
  { code: 'dmg_L', name: 'L = Lackschaden', state: true },
  { code: 'dmg_S', name: 'S = Steinschlag', state: true },
];

function CarIssues({ onSubmit, t }) {
  const [issues, setIssues] = useState([]);
  const [isOpen, setIsOpen] = useState(false);
  const [selectedPart, setSelectedPart] = useState([]);
  const [dataForApi, setDataForApi] = useState([])
  const navigation = useNavigation();
  const [snapShot, setSnapShot] = useState()
  const ref = useRef();

  useEffect(() => {
    takeSnapShot()
  }, [isOpen]);

  const takeSnapShot = () => {
    ref.current.capture().then(uri => {
      setSnapShot({ uri: uri })
    });
  }
  const getIssueStyle = (_issue) => {
    const styleMap = {
      dmg_M: styles.overlaySideMirror,
      dmg_F: styles.overlaybackLight,
      dmg_L: styles.overlayColorScratch,
      dmg_B: styles.overlayFendor,
      dmg_D: styles.overlayDent,
    };
    return styleMap[_issue];
  };
  const getIssueName = (_issueCode) => {
    const arr = ALL_ISSUES.filter((it) => it.code === _issueCode);
    return arr.length > 0 ? arr[0].name : '';
  };

  const handleSubmit = () => {
    let data = {
      car_issues: dataForApi,
      issues_snapshot: snapShot
    }
    onSubmit(data);
  };


  const handleImageTap = (selectedParts) => {
    const alreadyExist = selectedPart.find(item => item == selectedParts)
    if (alreadyExist) {
      let removeFromDataApi = dataForApi.filter(item => item.location_on_car !== selectedParts)

      let removeExistant = selectedPart.filter(item => item != selectedParts)
      setDataForApi(removeFromDataApi)
      setSelectedPart(removeExistant)
      return
    }
    setSelectedPart([...selectedPart, selectedParts]);
    setIsOpen(true);
  };
  const handleDone = () => {
    let currentSelectedPart = selectedPart[selectedPart.length - 1]
    let issuesWithName = []
    issues.map((item) => {
      let data = {
      }
      data.name = getIssueName(item)
      data.code = item
      data.location_on_car = currentSelectedPart
      issuesWithName.push(data)
    })
    setDataForApi([...dataForApi, ...issuesWithName])
  }

  return (
    <Box minHeight={'100%'}>
      <Box flex={0.85}>
        <Text alignSelf='center' my={1}>
          Click on image for options
        </Text>
        <CarCanvas ref={ref} callback={handleImageTap} selectedPart={selectedPart} />
        <Box mx={4}>
          <Text mt={5} style={{ fontSize: 20, fontWeight: 'bold' }}>
            Issues:
          </Text>
          <VStack space={1}>
            {dataForApi && issues.length > 0 ? (
              dataForApi.map((item, index) => (
                <Text key={index} style={styles.issueLabel}>
                  {item.location_on_car} : {item.name}
                </Text>
              ))
            ) : (
              <Text>No Issues in Car</Text>
            )}
          </VStack>
        </Box>
      </Box>

      <HStack mt={4} alignSelf={'flex-end'} justifyContent='space-around' w='100%'>
        <Button
          text={t('Save Order')}
          onPress={() => {
            navigation.navigate('HomeScreen');
          }}
        />
        <Button text={t('Continue')} onPress={handleSubmit} />
      </HStack>
      <Modal isOpen={isOpen} onClosed={() => setIsOpen(false)} style={styles.modal} position={'center'}>
        <Box style={styles.popupWindow}>
          <Pressable style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center' }}>
            <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 14 }}>Add Issues</Text>
          </Pressable>
          <Pressable>
            <Checkbox.Group
              defaultValue={[]}
              onChange={(values) => {
                setIssues(values || []);
              }}
            >
              {
                ALL_ISSUES.map((_issue) => (
                  <Checkbox isChecked={false} accessibilityLabel={_issue.name} value={_issue.code} my={1}>
                    {_issue.name}
                  </Checkbox>
                ))

              }
            </Checkbox.Group>

          </Pressable>
          <Button primary onPress={() => {
            handleDone()

            setIsOpen(false)
          }} text='Done' my={2} />
        </Box>
      </Modal>
    </Box>
  );
}

const styles = StyleSheet.create({
  circle: {
    width: 35,
    height: 35,
    borderRadius: 100 / 2,
    borderWidth: 4,
    borderColor: 'red',
    zIndex: 99999,
    position: 'absolute',
  },
  container: {
    alignItems: 'center',
    position: 'relative',
  },
  overlaySideMirror: {
    top: 50,
    right: 16,
  },
  overlaybackLight: {
    top: 60,
    left: 90,
    alignSelf: 'flex-end',
  },
  overlayColorScratch: {
    top: 160,
    right: 15,
    // alignSelf: 'center'
  },
  overlayFendor: {
    top: 60,
    right: 110,
  },
  overlayDent: {
    top: 160,
    left: 5,
    // alignSelf: 'center'
  },
  modal: {
    alignItems: 'center',
    backgroundColor: '#fff',
    height: 'auto',
    width: '90%',
    borderRadius: 20,
  },
  popupWindow: {
    width: '100%',
    marginTop: 8,
    marginRight: 16,
    marginLeft: 16,
    paddingLeft: 12,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    paddingRight: 12,
    color: 'black',
  },
});

CarIssues.propTypes = {
  onSubmit: PropTypes.function,
  t: PropTypes.function,
};

export const CarIssuesComponent = withTranslation()(CarIssues);
