import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, StyleSheet } from 'react-native';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import { VStack, HStack, Text, Box } from 'native-base';

function OrderCard({ onViewDetails, onAddCosting, continueOrder, item, cardType }) {
  return (
    <Box px={4} py={2} style={styles.listCard}>
      <HStack justifyContent='space-between'>
        <VStack>
          <Text fontSize='sm' color='gray.500'>
            {item.delivery_company_name}
          </Text>
          <Text fontSize='sm' color='gray.500'>
            {item.order_number}
          </Text>
        </VStack>
        <Text fontSize='xs' color='gray.500'>
          {item.date_of_pick_up}
        </Text>
      </HStack>
      <Text style={styles.bodyText}>
        {item.delivery_road} {item.place}, {item.delivery_postal_code}
      </Text>
      <Box style={{ alignItems: 'center', marginTop: 20, marginBottom: 8 }}>
        {cardType === 'accepted' ? (
          <TouchableOpacity onPress={onViewDetails} style={styles.footerContainer}>
            <Text style={styles.footerText}>View Details </Text>
            <EntypoIcon name={'chevron-right'} size={24} color={'#0b5ea2'} />
          </TouchableOpacity>
        ) : cardType === 'in-progress' ? (
          <Box style={styles.inProgressFooter}>
            <TouchableOpacity onPress={onAddCosting} style={styles.footerContainer}>
              <Text style={styles.footerText}>Add Costing </Text>
              <EntypoIcon name={'plus'} size={24} color={'#0b5ea2'} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.footerContainer} onPress={continueOrder}>
              <Text style={styles.footerText}>Continue</Text>
              <EntypoIcon name={'chevron-right'} size={24} color={'#0b5ea2'} />
            </TouchableOpacity>
          </Box>
        ) : (
          <></>
        )}
      </Box>
    </Box>
  );

}

OrderCard.propTypes = {
  onViewDetails: PropTypes.func,
  onAddCosting: PropTypes.func,
  item: PropTypes.any,
  cardType: PropTypes.string,
  continueOrder: PropTypes.func,
};

const styles = StyleSheet.create({
  listCard: {
    margin: 6,
    backgroundColor: '#FFFFFF',
    borderRadius: 18,
    shadowOffset: { width: 0.95, height: 0.95 },
    shadowColor: '#a2a2a2',
    shadowOpacity: 0.5,
    shadowRadius: 2,
  },
  footerContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  footerText: {
    color: '#0b5ea2',
  },
  bodyText: {
    marginTop: 4,
    fontSize: 16,
  },
  inProgressFooter: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    paddingRight: '10%',
    paddingLeft: '10%',
  },
});

export { OrderCard };
