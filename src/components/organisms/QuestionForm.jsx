import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'src/components/molecules';
import { Box, VStack, Text, Radio, Checkbox } from 'native-base';
import QuestionPopup from '../molecules/QuestionPopup';

export function QuestionForm({ question, onSubmit, onPhotoSubmit }) {
  const [tempSelectedValue, setTempSelectedValue] = useState()
  const [popupVisible, setPopupVisible] = useState(false)
  const [images, setImages] = useState([])
  const [checkboxValues, setCheckBoxvalues] = useState()
  function submit(option) {
    onSubmit(option);
  }
  const onButtonPress = (item) => {
    if (item?.action) {
      setPopupVisible(true)
      setTempSelectedValue(item)
    } else {
      let data = {
        option: item.value
      }
      onSubmit(data)
    }
  }

  const onCheckBoxPress = (checkbox, item) => {
    if (item?.action) {
      setPopupVisible(checkbox)
    }
    setTempSelectedValue(item)
  }

  const onImagePopupsubmit = (values) => {
    setImages([...images, values])
  }

  const onPopupSubmit = (value) => {
    let data = {
      option: tempSelectedValue.value,
      note: value
    }
    onSubmit(data)
  }

  const onNextPress = () => {
    let data = {
      option: checkboxValues.join()
    }
    if (tempSelectedValue.action) {
      onPhotoSubmit(images)
    }
    onSubmit(data)
  }
  return (
    <Box p={10} rounded={10} width='100%'>
      <QuestionPopup
        selectedOption={tempSelectedValue} onPopupSubmit={onPopupSubmit}
        popupVisible={popupVisible} setPopupVisible={setPopupVisible}
        setImages={setImages} onImagePopupsubmit={onImagePopupsubmit}

      />
      <VStack space={2}>
        <Text fontSize='md' textAlign='center' alignSelf='center'>
          {question?.question}
        </Text>
        {
          question?.question_type == "checkbox" ?

            <Checkbox.Group onChange={(values) => setCheckBoxvalues(values)} >
              {question.options?.map((it) => (
                <Checkbox
                  onChange={(value) => onCheckBoxPress(value, it)}
                  accessibilityLabel='test' key={it.label} value={it.label} mx={1} my={0.5}>
                  {it.label}
                </Checkbox>
              ))}
            </Checkbox.Group> :

            question?.options?.map((item) => <Button
              primary
              elevation={2}
              _hover={{ bg: '#23619C' }}
              variant='ghost'
              onPress={() => onButtonPress(item)}
              bordered
              text={item.label}
            />)
        }

        {
          question?.question_type == "checkbox" &&
          <Button
            primary
            elevation={2}
            _hover={{ bg: '#23619C' }}
            variant='ghost'
            onPress={() => onNextPress()}
            bordered
            text={"Submit"}
          />
        }
        {
          question?.is_skippable == "1" &&
          <Button
            primary
            elevation={2}
            _hover={{ bg: '#23619C' }}
            variant='ghost'
            onPress={() => onButtonPress("Skipped")}
            bordered
            text={"Skip"}
          />
        }

      </VStack>
    </Box>
  );
}

QuestionForm.propTypes = {
  onSubmit: PropTypes.func,
  question: PropTypes.string,
};
