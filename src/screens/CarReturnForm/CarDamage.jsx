import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'src/components/molecules';
import { Box, VStack, Text } from 'native-base';
import { withTranslation } from 'react-i18next';

function CarDamage({ onSubmit }) {
  function submit(val) {
    onSubmit({ inner_space: val });
  }
  return (
    <Box m={10} bg='white' p={10} rounded={10}>
      <VStack space={2}>
        <Text alignSelf='center'>Inner Space</Text>
        <Button
          primary
          elevation={2}
          _hover={{ bg: '#23619C' }}
          variant='ghost'
          onPress={() => submit('missing')}
          bordered
          text='MISSING'
        />
        <Button
          primary
          elevation={2}
          _hover={{ bg: '#23619C' }}
          variant='ghost'
          onPress={() => submit('damaged')}
          bordered
          text='DAMAGE'
        />
        <Button
          primary
          elevation={2}
          _hover={{ bg: '#23619C' }}
          variant='ghost'
          onPress={() => submit('dirty')}
          bordered
          text='DIRTY'
        />
      </VStack>
    </Box>
  );
}

CarDamage.propTypes = {
  onSubmit: PropTypes.func,
};

export default withTranslation()(CarDamage);
