import { ImageBackground, StyleSheet, TouchableOpacity, View } from 'react-native';
import React from 'react';
import CarIssuesPng from 'src/assets/car-issues.png';
import { vh, vw } from '../../theme';
import ViewShot from 'react-native-view-shot';

const CarCanvas = React.forwardRef((props, ref) => {
  const RedCircle = () => {
    return <View style={styles.circle} />;
  };
  const handleOnCarPress = (item) => {
    props.callback(item);
  };
  const isExisted = (element) => {
    return props.selectedPart.some(item => item == element)
  }
  return (
    <ViewShot ref={ref} style={styles.container}>
      <ImageBackground source={CarIssuesPng} imageStyle={styles.carImageStyle} style={styles.imageContainer}>
        <View style={styles.upsideDownCar}>
          <TouchableOpacity onPress={() => handleOnCarPress('Right Front fender')} style={styles.carTire}>
            {isExisted('Right Front fender') && <RedCircle />}
          </TouchableOpacity>
          <TouchableOpacity onPress={() => handleOnCarPress('Right Front Door')} style={styles.frontDoor}>
            {isExisted('Right Front Door') && <RedCircle />}
          </TouchableOpacity>
          <TouchableOpacity onPress={() => handleOnCarPress('Right Back Door')} style={styles.backDoor}>
            {isExisted('Right Back Door') && <RedCircle />}
          </TouchableOpacity>
          <TouchableOpacity onPress={() => handleOnCarPress('Right Rear fender')} style={styles.carTire}>
            {isExisted('Right Rear fender') && <RedCircle />}
          </TouchableOpacity>
        </View>
        <View style={styles.midImageContainer}>
          <TouchableOpacity onPress={() => handleOnCarPress('Car Front')} style={styles.carFront}>
            <TouchableOpacity onPress={() => handleOnCarPress('Front Bumper')} style={styles.frontBumper}>
              {isExisted('Front Bumper') && <RedCircle />}
            </TouchableOpacity>
          </TouchableOpacity>
          <View style={styles.topOfCar}>
            <TouchableOpacity onPress={() => handleOnCarPress('Front Bonnet')} style={styles.topBonnet}>
              {isExisted('Front Bonnet') && <RedCircle />}
            </TouchableOpacity>
            <TouchableOpacity onPress={() => handleOnCarPress('WindSheild')} style={styles.windSheild}>
              {isExisted('WindSheild') && <RedCircle />}
            </TouchableOpacity>
            <TouchableOpacity onPress={() => handleOnCarPress('Roof')} style={styles.topRoof}>
              {isExisted('Roof') && <RedCircle />}
            </TouchableOpacity>
            <TouchableOpacity onPress={() => handleOnCarPress('back glass')} style={styles.backGlass}>
              {isExisted('back glass') && <RedCircle />}
            </TouchableOpacity>
          </View>
          <View style={styles.backOfCar}>
            <TouchableOpacity onPress={() => handleOnCarPress('Back Bumper')} style={styles.backbumper}>
              {isExisted('Back Bumper') && <RedCircle />}
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.upsideDownCar}>
          <TouchableOpacity onPress={() => handleOnCarPress('Front Left fender')} style={styles.sidecarTire}>
            {isExisted('Front Left fender') && <RedCircle />}
          </TouchableOpacity>
          <TouchableOpacity onPress={() => handleOnCarPress('Left Front Door')} style={styles.frontDoor}>
            {isExisted('Left Front Door') && <RedCircle />}
          </TouchableOpacity>
          <TouchableOpacity onPress={() => handleOnCarPress('Left Back Door')} style={styles.backDoor}>
            {isExisted('Left Back Door') && <RedCircle />}
          </TouchableOpacity>
          <TouchableOpacity onPress={() => handleOnCarPress('Rear Left fender')} style={styles.sidecarTire}>
            {isExisted('Rear Left fender') && <RedCircle />}
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </ViewShot >
  );
});

export default CarCanvas;

const styles = StyleSheet.create({
  container: {
    width: vw * 100,
    height: vh * 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageContainer: {
    width: '90%',
    height: '100%',
  },
  carImageStyle: {
    width: '100%',
    height: '100%',
    resizeMode: 'stretch',
  },
  upsideDownCar: {
    height: '30%',
    width: '67%',
    alignSelf: 'center',
    flexDirection: 'row',

    alignItems: 'center',
    paddingLeft: '5%',
  },
  carTire: {
    height: '25%',
    width: '20%',
    top: vh * 0.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  frontDoor: {
    height: '55%',
    width: '25%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  backDoor: {
    height: '55%',
    width: '20%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  midImageContainer: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    height: '40%',
  },
  carFront: {
    height: '100%',
    width: '20%',
    left: vw * 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  frontBumper: {
    height: '80%',
    width: '45%',
    left: vw * 4,
    alignItems: 'center',
    justifyContent: 'center',
  },
  topOfCar: {
    height: '100%',
    width: '55%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  topBonnet: {
    height: '80%',
    width: '25%',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: vw * 5,
    borderBottomLeftRadius: vw * 5,
  },
  windSheild: {
    height: '60%',
    alignItems: 'center',
    justifyContent: 'center',
    width: '15%',
  },
  topRoof: {
    height: '60%',
    width: '35%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  backGlass: {
    height: '65%',
    width: '25%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  backOfCar: {
    height: '100%',
    width: '20%',
    right: '2%',
    flexDirection: 'row',

  },
  backbumper: {
    height: '80%',
    width: '45%',
    left: vw * 5,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  sideOfCar: {
    height: '30%',
    width: '50%',
    left: vw * 2,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'green',
  },
  sidecarTire: {
    height: '25%',
    width: '20%',
    bottom: vh * 0.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  circle: {
    width: vh * 3,
    height: vh * 3,
    borderRadius: 100 / 2,
    borderWidth: 2,
    borderColor: 'red',
  },
});
