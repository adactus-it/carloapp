import AsyncStorage from '@react-native-async-storage/async-storage';

const storeData = async (key, val) => {
  try {
    AsyncStorage.setItem(key, JSON.stringify(val));
  } catch (e) {
    // console.error({ e });
  }
};

const getData = async (key) => {
  try {
    const val = await AsyncStorage.getItem(key);
    if (val !== null) {
      return JSON.parse(val);
    }
    throw new Error('null value');
  } catch (e) {
    // console.error({ e });
  }
};

const clearStorage = async () => {
  try {
    await AsyncStorage.clear();
  } catch (e) {
    // clear error
  }
};

export { storeData, getData, clearStorage };
