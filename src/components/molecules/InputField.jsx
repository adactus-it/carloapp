import { StyleSheet, Text, View, TextInput } from 'react-native';
import React from 'react';
import { borderColor } from 'styled-system';
import { TextArea } from 'native-base';

const InputField = (props) => {
  return (
    <View style={styles.container}>
      {/* <TextInput
        placeholder={props.placeholder}
        {...props}
/> */}
      <TextArea {...props} />
    </View>
  );
};

export default InputField;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 40,
    marginVertical: 10,
  },
});
