import React, { useState, useContext } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

import LoginComponent from '../screens/Login';
import HomeComponent from '../screens/Drawer';
import OrderDetailsComponent from '../screens/OrderDetails';
import DriverFormComponent from '../screens/DriverForm/index';
import CustomerFormComponent from '../screens/CustomerForm/index';
import CarReturnFormComponent from '../screens/CarReturnForm/index';
import CostingComponent from '../screens/Costing';
import DriverScheduleComponent from '../screens/DriverSchedule';
import { AppContext } from 'src/store';
import SplashScreen from '../screens/SplashScreen';
import { CarIssues } from '../components/organisms';

const Stack = createStackNavigator();

export const ROUTES = {
  home: 'HomeScreen',
  schedule: 'DriverSchedule',
  costing: 'Costing',
  orderDetails: 'OrderDetails',
  driverForm: 'DriverForm',
  customerForm: 'CustomerForm',
  returnForm: 'CarReturn',
};

function AppStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#23619C',
        },
        headerTintColor: '#FFF',
      }}
    >
      <Stack.Screen options={{ headerShown: false }} name='HomeScreen' component={HomeComponent} />
      <Stack.Screen name='CarReturn' component={CarReturnFormComponent} />
      <Stack.Screen name='CustomerForm' component={CustomerFormComponent} />
      <Stack.Screen initial name='DriverForm' component={DriverFormComponent} options={{ params: { orderId: 3926 } }} />
      <Stack.Screen name='Costing' component={CostingComponent} />
      <Stack.Screen name='OrderDetails' component={OrderDetailsComponent} />
    </Stack.Navigator>
  );
}

function Router() {
  const { isSignedIn } = useContext(AppContext);
  const [loading, setLoading] = useState(true);

  React.useEffect(() => {
    if (isSignedIn === undefined) {
      setLoading(true);
    } else
      setTimeout(() => {
        setLoading(false);
      }, 2000);
  }, [isSignedIn]);
  return loading ?
    <SplashScreen />
    : (
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        {!isSignedIn() ? (
          <Stack.Screen name='Login' component={LoginComponent} />
        ) : (
          <Stack.Screen name='Home' component={AppStack} />
        )}
      </Stack.Navigator>
    );
}

const RootStack = createStackNavigator();

function RootStackScreen() {
  return (
    <NavigationContainer>
      <RootStack.Navigator mode='modal'>
      {/* <RootStack.Screen name='Main' component={CarIssues} options={{ headerShown: false }} /> */}
        <RootStack.Screen name='Main' component={Router} options={{ headerShown: false }} />
        <RootStack.Screen name='DriverSchedule' component={DriverScheduleComponent} />
      </RootStack.Navigator>
    </NavigationContainer>
  );
}

export default RootStackScreen;
