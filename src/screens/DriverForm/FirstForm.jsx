import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

import { StyleSheet, ScrollView, View } from 'react-native';
import { HStack, VStack, Text, Radio } from 'native-base';
import { withTranslation } from 'react-i18next';
import { FormControl, Button } from 'src/components/molecules';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { parseDateString } from 'src/utils';

const schema = yup.object().shape({
  start_time: yup
    .date()
    .transform(parseDateString)
    // .min(new Date(), `Start time must be later than ${format(new Date(), 'HH:mm')}`)
    .required(),
  end_time: yup
    .date()
    .transform(parseDateString)
    // .min(new Date(), `End time must be later than ${format(new Date(), 'HH:mm')}`)
    .required(),
  vehicle_refueling: yup.string().required(),
  mileage: yup.number().required().typeError('Not a valid number'),
  remarks: yup.string()

});

function FirstForm({ t, onSubmit, data }) {
  const {
    control,
    formState: { errors },
    handleSubmit,
  } = useForm({
    resolver: yupResolver(schema),
    defaultValues: {
      start_time: new Date(),
      end_time: new Date(),
      vehicle_refueling: data.vehicle_refueling,
      remarks: "",
      mileage: data.mileage,
    },
  });
  const FUEL_GAUGES = ['1/1', '3/4', '1/2', '1/4', 'reserve'];

  const inputRef = useRef(null);

  return (
    <ScrollView
      contentContainerStyle={{
        width: '100%',
        marginTop: 100,
        alignSelf: 'center',
      }}
    >
      <VStack space={2} mx={4}>
        <Text style={styles.headingText}>{t('WAITING TIME FOR COLLECTION')}</Text>
        <HStack mt={10} space={2} align='center' justifyContent='space-between'>
          <FormControl
            control={control}
            name='start_time'
            label='Start time'
            placeholder='Select start date'
            error={errors?.start_time}
            isRequired={true}
            type='date'
          />
          <FormControl
            control={control}
            isRequired={true}
            name='end_time'
            error={errors?.end_time}
            label='End time'
            placeholder='Select end date'
            type='date'
          />
        </HStack>
        <View style={{ marginVertical: 20 }}>
          <Text alignSelf='center'  >Vehicle Refuling</Text>
          <FormControl block control={control} name={'vehicle_refueling'} error={errors?.vehicle_refueling}>
            {({ field: { onChange, value } }) => (
              <Radio.Group
                accessibilityLabel='test'
                justifyContent='space-around'
                flexDirection='row'
                flexWrap='wrap'
                name='vehicle_refueling'
                error={errors?.vehicle_refueling}
                onChange={onChange}
                value={value}
              >
                {FUEL_GAUGES.map((it) => (
                  <Radio accessibilityLabel='test' key={it} value={it} mx={1} my={0.5}>
                    {it}
                  </Radio>
                ))}
              </Radio.Group>
            )}

          </FormControl>
        </View>

        <FormControl
          block
          forwardedRef={inputRef}
          returnKeyType={'done'}
          onSubmitEditing={handleSubmit(onSubmit)}
          control={control}
          isRequired={true}
          name='mileage'
          keyboardType='numeric'
          error={errors?.mileage}
          textContentType='number'
          label='Mileage on collection'
        />
        <FormControl
          block
          forwardedRef={inputRef}
          returnKeyType={'done'}
          onSubmitEditing={handleSubmit(onSubmit)}
          control={control}
          h={20}
          name='remarks'
          error={errors?.remarks}
          label='Remarks'
        />

      </VStack>
      <HStack justifyContent='space-around' w='100%' my={4}>
        <Button text={t('Save Order')} onPress={() => {
        }} />
        <Button text={t('Continue')} onPress={handleSubmit(onSubmit)} />
      </HStack>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  headingText: {
    color: '#0b5ea2',
    textAlign: 'center',
  },
});

FirstForm.propTypes = {
  t: PropTypes.func,
  navigation: PropTypes.object,
  onSubmit: PropTypes.func,
  data: PropTypes.object,
};

export default withTranslation()(FirstForm);
