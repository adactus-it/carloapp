import { request, toFormData, makeRequest, imageToFormData } from 'src/utils';

const getCollectionUrl = (orderId) => `orders/${orderId}/customer_forms`;
const getPdfUrl = (orderId) => `generate_pdf/${orderId}`;

export function getCustomerFormForOrder(orderId) {
  return request({
    url: getCollectionUrl(orderId),
  });
}

export function updateCustomerFormForOrder(orderId, data) {
  return makeRequest('post', getCollectionUrl(orderId), toFormData(data), {
    headers: {
      'Content-Type': 'application/json',
    },
  });
}

export function getCustomerFormPdf(orderId) {
  return makeRequest('get', getPdfUrl(orderId));
}

export function updateCustomerFormCarIssues(customerFormid, data) {
  let formdata = new FormData();
  formdata.append('car_issues', JSON.stringify(data.car_issues));
  return makeRequest(
    'post',
    `customer_forms/${customerFormid}/car_issues`,
    formdata,
  );
}

export function updateCustomerFormMedia(customerFormid, data) {
  data = imageToFormData('media', data);
  return makeRequest('post', `customer_forms/${customerFormid}/media`, (data), {
    headers: {
      'content-type': 'application/json',
    },
  });
}
