import { request, toFormData, makeRequest, multipleImageToFormData } from 'src/utils';

const getCollectionUrl = (orderId) => `orders/${orderId}/driver_forms`;
const getPdfUrl = (orderId) => `generate_pdf/${orderId}`;

export function getDriverFormForOrder(orderId) {
  return request({
    url: getCollectionUrl(orderId),
  });
}

export function updateDriverFormForOrder(orderId, data) {
  return makeRequest('post', getCollectionUrl(orderId), toFormData(data));
}

export function getDriverFormPdf(orderId) {
  return makeRequest('get', getPdfUrl(orderId));
}

export function updateDriverFormCarIssues(driverFormId, data) {
  let formdata = new FormData();
  formdata.append('car_issues', JSON.stringify(data.car_issues));

  return makeRequest('post', `/driver_forms/${driverFormId}/car_issues`, formdata);
}

export function updateDriverFormMedia(driverFormId, data, form_type) {
  data = multipleImageToFormData('media[]', data);
  return makeRequest('post', `/bulkupload/${driverFormId}/${form_type}`, data);
}

export function getQuestions() {
  return makeRequest('get', '/questions');
}

export function answerQuestion(driverFormId, questionId, option) {
  const data = {
    question_id: questionId,
    ...option,
  };
  return makeRequest('post', `/driver_forms/${driverFormId}/questions`, toFormData(data));
}


export function submitDriverSignature(driverFormId, data) {
  return makeRequest('post', getCollectionUrl(driverFormId), toFormData(data), {
    headers: {
      'Content-Type': 'application/json',
    },
  });
}

