import { toFormData, makeRequest } from 'src/utils';

const getCollectionUrl = '/auth';

export function login(data) {
  return makeRequest('post', `${getCollectionUrl}/login`, toFormData(data), {
    headers: {
      'Content-Type': 'multipart/formdata',
    },
  });
}
