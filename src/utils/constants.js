import { Dimensions } from 'react-native';

export const ROUTES = {
  home: 'HomeScreen',
  schedule: 'DriverSchedule',
  costing: 'Costing',
  orderDetails: 'OrderDetails',
  driverForm: 'DriverForm',
  customerForm: 'CustomerForm',
  returnForm: 'CarReturn',
};

export const technical_defects = [
  {
    id: 1,
    name: 'Roof Lining',
    value: false,
    note: false,
  },
  {
    id: 2,
    name: 'Interior Trim',
    value: false,
    note: false,
  },
  {
    id: 3,
    name: 'Radio',
    value: false,
    note: false,
  },
  {
    id: 4,
    name: 'Rear seat',
    value: false,
    note: false,
  },
  {
    id: 5,
    name: 'Driver seat',
    value: false,
    note: false,
  },
  {
    id: 6,
    name: 'Passenger seat',
    value: false,
    note: false,
  },
  {
    id: 7,
    name: 'Trunk / Cargo area',
    value: false,
    note: false,
  },
  {
    id: 8,
    name: 'Tools',
    value: false,
    note: false,
  },
  {
    id: 9,
    name: 'Front Floor Covering',
    value: false,
    note: false,
  },
  {
    id: 10,
    name: 'Dashboard',
    value: false,
    note: false,
  },
  {
    id: 11,
    name: 'Center Console',
    value: false,
    note: false,
  },
];

export const DB_Questions = [
  {
    question: 'Fahrgetellnr stimmit mit Kfz-Schein / Fahrzeug / Serviceheft überein',
    options: [
      {
        label: 'ja',
        value: 'Ja',
      },
      {
        label: 'nien',
        value: 'Nien',
        action: {
          popup: {
            inputType: 'text',
            inputLabel: 'Enter VIN number',
          },
        },
      },
    ],
  },
  {
    question: 'Kennzeichen stimmen mit Kfz-Schein und am Fahrzeug überein',
    key_name: 'license_number',
    options: [
      {
        label: 'ja',
        value: 'Ja',
      },
      {
        label: 'nien',
        value: 'Nien',
        action: {
          popup: {
            inputType: 'text',
            inputLabel: 'Enter License Plate',
          },
        },
      },
    ],
  },
  {
    question: 'Alle Schluessel passen zum Fahrzeug',
    key_name: 'car_keys_fit',
    options: [
      {
        label: 'ja',
        value: 'Ja',
      },
      {
        label: 'nien',
        value: 'Nien',
      },
    ],
  },
  {
    question: 'Sichtprüung Außen?/Visual inspection from the outside?',
    key_name: 'visual_inspection',
    options: [
      {
        label: 'ja',
        value: 'Ja',
      },
      {
        label: 'nien',
        value: 'Nien',
        action: {
          inputType: 'select',
          inputLabel: 'Why not?',
          inputOptions: ['Pollution of the vehicle', 'Darkness', 'Rain / Wet / Snow / Ice', 'Underground car park'],
        },
      },
    ],
  },
  {
    question: 'Sichtprufund innen (Verschmutzune, Aufereitungsreste, Schuhsohlenstreifen)',
    key_name: 'defects',
    questionType: 'checkbox',
    isSkippable: true,
    options: [
      {
        label: 'Seats',
        value: 'seats',
        action: {
          popup: {
            inputType: 'image',
            inputLabel: 'Add image of defect',
          },
        },
      },
      {
        label: 'Foot Space',
        value: 'foot_space',
        action: {
          popup: {
            inputType: 'image',
            inputLabel: 'Add image of defect',
          },
        },
      },
      {
        label: 'Amatures',
        value: 'amatures',
        action: {
          popup: {
            inputType: 'image',
            inputLabel: 'Add image of defect',
          },
        },
      },
      {
        label: 'Trunk',
        value: 'trunk',
        action: {
          popup: {
            inputType: 'image',
            inputLabel: 'Add image of defect',
          },
        },
      },
    ],
  },
  {
    question: 'Bordbuch mit Auslieferungsdatum',
    key_name: 'car_book',
    options: [
      {
        label: 'Present (Physically)',
        value: 'Ja',
      },
      {
        label: 'Not Present',
        value: 'nien',
      },
      {
        label: 'Electronic Logbook',
        value: 'electronic',
      },
    ],
  },
  {
    question: 'Navigation funktionsfahig',
    key_name: 'car_book',
    options: [
      {
        label: 'Present (Physically)',
        value: 'Ja',
      },
      {
        label: 'Not Present',
        value: 'nien',
      },
      {
        label: 'Navi not present',
        value: 'not_present',
      },
    ],
  },
  {
    question: 'Accessories',
    key_name: 'accessories',
    options: [
      {
        label: 'Warndreieck',
      },
      { label: 'Erste Hilfe' },
      { label: 'Warnweste ' },
      { label: 'Grüne Umweltplakette' },
      { label: 'Satz Reifen' },
      { label: 'Tank-/Servicekarten' },
      { label: 'Adapterkabel ' },
    ],
  },
];
