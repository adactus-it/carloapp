import React from 'react';
import PropTypes from 'prop-types';

import { FormControl as FC, Input, Box, TextArea, Select, CheckIcon, Text } from 'native-base';
import { Controller } from 'react-hook-form';
import DatePicker from 'react-native-datepicker';

function FormControl({
  block,
  label,
  error,
  placeholder,
  options,
  type,
  control,
  name,
  isRequired,
  rules,
  defaultValue,
  mode,
  forwardedRef,
  children,
  ...props
}) {
  const renderInput = (type, onChange, value, forwardedRef) => {
    switch (type) {
      case 'date':
        return (
          <DatePicker
            ref={forwardedRef}
            date={value}
            mode={mode}
            showIcon={false}
            format='HH:mm'
            placeholder={placeholder}
            confirmBtnText='Confirm'
            cancelBtnText='Cancel'
            minDate={new Date()}
            customStyles={{
              dateInput: {
                borderWidth: 0,
                borderBottomWidth: 1,
                alignItems: 'flex-start',
              },
            }}
            onDateChange={onChange}
            {...props}
          />
        );
      case 'textarea':
        return <TextArea value={value} onChangeText={onChange} {...props} ref={forwardedRef} />;
      case 'select':
        return (
          <Select
            ref={forwardedRef}
            selectedValue={value}
            accessibilityLabel={placeholder}
            placeholder={placeholder}
            onValueChange={onChange}
            _selectedItem={{
              bg: 'primary.500',
              endIcon: <CheckIcon size={4} />,
            }}
            alignSelf={'center'}
            w={'90%'}
            {...props}
          >
            {options.map((it) => (
              <Select.Item key={it} label={it} value={it} />
            ))}
          </Select>
        );
      default:
        return <Input ref={forwardedRef} p={2} type={type} placeholder={placeholder} onChangeText={onChange}
                      value={`${value}`} {...props} />;
    }
  };

  return (
    <FC w={block ? '100%' : 150} borderWidth={0} isRequired={isRequired} isInvalid={error != null}>
      <Box>
        <FC.Label>{label}</FC.Label>
        <Controller
          control={control}
          render={children || (({
            field: {
              onChange,
              value,
            },
          }) => renderInput(type, onChange, value, forwardedRef))}
          name={name}
          rules={rules}
          defaultValue={defaultValue || ''}
        />
        <FC.ErrorMessage>
          {
            <Text color='red.500' fontSize='sm'>
              {error?.message}
            </Text>
          }
        </FC.ErrorMessage>
      </Box>
    </FC>
  );
}

FormControl.propTypes = {
  name: PropTypes.string.isRequired,
  control: PropTypes.func.isRequired,
  label: PropTypes.string,
  type: PropTypes.string,
  rules: PropTypes.object,
  defaultValue: PropTypes.string,
  placeholder: PropTypes.string,
  error: PropTypes.string,
  isRequired: PropTypes.bool,
  block: PropTypes.bool,
  mode: PropTypes.string,
  options: PropTypes.array,
  forwardedRef: PropTypes.object,
  children: PropTypes.element,
};

FormControl.defaultProps = {
  options: [],
  isRequired: false,
  mode: 'time',
};

export { FormControl };
