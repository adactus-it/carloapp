import React, { useState, useEffect, useContext } from 'react';
import { StyleSheet } from 'react-native';
import { OrderCard } from 'src/components/organisms';
import PropTypes from 'prop-types';
import { Box, IconButton, HamburgerIcon, FlatList, useToast, Text } from 'native-base';
import SegmentedControlTab from 'react-native-segmented-control-tab';
import { withTranslation } from 'react-i18next';
import { showApiErrorToast } from 'src/utils';
import { AppContext } from 'src/store';
import { fetchOrders } from 'src/api/orders';
import { getDriverSchedule } from '../api/driver';
import { useTranslation } from 'react-i18next';

function AppHome({ navigation }) {
  const [tabIndex, setTabIndex] = useState(0);
  const [openOrders, setOpenOrders] = useState([]);
  const [ongoingOrders, setOngoingOrders] = useState([]);
  const { t } = useTranslation();

  const toast = useToast();
  const { setLoading, loading, setDriverSchedule } = useContext(AppContext);

  const fetchData = async () => {
    try {
      setLoading(true);
      const { new_orders, in_progress_orders } = await fetchOrders();
      setOpenOrders(new_orders);
      setOngoingOrders(in_progress_orders);
    } catch (err) {
      showApiErrorToast(err, toast);
    } finally {
      setLoading(false);
    }
  };

  async function verifyDriverSchedule() {
    try {
      const { driver_schedule } = await getDriverSchedule();
      if (driver_schedule)
        setDriverSchedule(driver_schedule);
    } catch (e) {
      showApiErrorToast(e, toast);
      setDriverSchedule(null);
      navigation.navigate('DriverSchedule');
    }
  }

  useEffect(() => {
    const hamburgerIcon = () => (
      <IconButton
        icon={<HamburgerIcon color='#fff' />}
        transparent
        onPress={() => {
          navigation.openDrawer();
        }}
        style={{ color: '#fff' }}
      />
    );
    navigation.setOptions({
      headerLeft: hamburgerIcon,
    });
    // verifyDriverSchedule();
    fetchData();
  }, [navigation]);
  const onTabChange = (index) => {
    setTabIndex(index);
  };

  const orderDetails = (order) => {
    navigation.navigate('OrderDetails', { order });
  };

  const addCosting = (orderId) => {
    navigation.navigate('Costing', { orderId });
  };

  const continueOrder = (order) => {
    if (order.form_stage === 'customer_form') navigation.navigate('CustomerForm', { orderId: order.id });
    else if (order.form_stage === 'car_return_form') navigation.navigate('CarReturn', { orderId: order.id });
    else navigation.navigate('DriverForm', { orderId: order.id });
  };

  return (
    <Box style={homeStyles.container}>
      <SegmentedControlTab
        values={[t('accepted_orders'), t('inprogress_orders')]}
        selectedIndex={tabIndex}
        onTabPress={onTabChange}
        borderRadius={8}
        tabsContainerStyle={homeStyles.container2}
        tabStyle={homeStyles.commonStyle}
        activeTabStyle={{ ...homeStyles.commonStyle, ...homeStyles.activeStyle }}
        tabTextStyle={homeStyles.text2}
        activeTabTextStyle={homeStyles.text2}
      />
      <FlatList
        data={tabIndex === 0 ? openOrders : ongoingOrders}
        numColumns={1}
        showsVerticalScrollIndicator={false}
        ListEmptyComponent={
          <Text alignSelf='center' mt={10}>
            {t("nothing_to_show")}
          </Text>
        }
        onRefresh={fetchData}
        refreshing={!!loading}
        renderItem={({ item, index }) => (
          <OrderCard
            key={index}
            item={item}
            onViewDetails={() => {
              orderDetails(item);
            }}
            onAddCosting={() => {
              addCosting(item.id);
            }}
            continueOrder={() => {
              continueOrder(item);
            }}
            cardType={tabIndex === 0 ? 'accepted' : 'in-progress'}
          />
        )}
      />
    </Box>
  );
}

AppHome.propTypes = {
  navigation: PropTypes.object.isRequired,
  t: PropTypes.func.isRequired,
  onDrawerClick: PropTypes.func,
};

const homeStyles = StyleSheet.create({
  container: {
    width: '100%',
    marginTop: 4,
    backgroundColor: '#f4f4f4',
    height: '100%',
  },
  container2: {
    height: 36,
    borderColor: '#eeeef0',
    backgroundColor: '#eeeef0',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    borderRadius: 8,
    overflow: 'hidden',
  },
  commonStyle: {
    backgroundColor: '#eeeef0',
    height: 30,
    borderTopRightRadius: 8,
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
    padding: 2,
    margin: 5,
    borderColor: 'transparent',
    borderWidth: 0,
  },
  activeStyle: {
    backgroundColor: '#fff',
    shadowOffset: { width: 0.95, height: 0.95 },
    shadowColor: '#a2a2a2',
    shadowOpacity: 0.5,
    shadowRadius: 2,
  },
  text2: {
    color: '#444444',
    fontWeight: 'bold',
  },
});

export default withTranslation()(AppHome);
