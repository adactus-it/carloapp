import React, { useContext } from 'react';
import { Spinner, Box, Text } from 'native-base';
import { PropTypes } from 'prop-types';
import { StyleSheet } from 'react-native';
import { AppContext } from 'src/store';

export function LoadingOverlay({ textContent }) {
  const { loading } = useContext(AppContext);

  return loading ? (
    <Box style={styles.background}>
      <Spinner color={'primary.500'} size={'lg'} />
      <Box style={[styles.textContainer]}>
        <Text style={[styles.textContent]}>{textContent}</Text>
      </Box>
    </Box>
  ) : (
    <></>
  );
}

LoadingOverlay.propTypes = {
  textContent: PropTypes.string,
};

LoadingOverlay.defaultProps = {
  textContent: '',
};

const styles = StyleSheet.create({
  activityIndicator: {
    flex: 1,
  },
  background: {
    alignItems: 'center',
    bottom: 0,
    justifyContent: 'center',
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
    zIndex: 99,
    backgroundColor: 'rgba(0, 0, 0, 0.25)',
  },
  container: {
    backgroundColor: 'transparent',
    bottom: 0,
    flex: 1,
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
  },
  textContainer: {
    alignItems: 'center',
    bottom: 0,
    flex: 1,
    justifyContent: 'center',
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
  },
  textContent: {
    fontSize: 20,
    fontWeight: 'bold',
    height: 50,
    top: 80,
    color: 'white',
  },
});
