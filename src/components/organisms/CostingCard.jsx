import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Box, Text } from 'native-base';
import EntypoIcon from 'react-native-vector-icons/Entypo';

const CostingCard = ({ costing, onDelete, ...props }) => {
  return (
    <Box style={styles.card} {...props}>
      <Box style={{ padding: 10, display: 'flex', flexDirection: 'column' }}>
        <Text>{costing.name}</Text>
        <Text>{costing.amount}</Text>
      </Box>
      <TouchableOpacity onPress={() => onDelete(costing.id)}>
        <EntypoIcon style={styles.crossIcon} name={'cross'} size={28} color={'#000'} />
      </TouchableOpacity>
    </Box>
  );
};

CostingCard.propTypes = {
  costing: PropTypes.object,
  onDelete: PropTypes.func,
};

const styles = StyleSheet.create({
  card: {
    margin: 6,
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 12,
    shadowOffset: { width: 0.95, height: 0.95 },
    shadowColor: '#a2a2a2',
    shadowOpacity: 0.5,
    shadowRadius: 2,
    marginLeft: 15,
    marginRight: 15,
  },
  crossIcon: {
    marginRight: 6,
  },
});

export { CostingCard };
