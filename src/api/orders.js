import { makeRequest } from 'src/utils';

export async function fetchOrders() {
  const response = await makeRequest('get', 'orders');
  const { body: { in_progress_orders, new_orders } } = response;
  return { in_progress_orders, new_orders };
}

export async function fetchCompletedOrders() {
  const response = await makeRequest('get', 'orders/completed');
  const { body: { orders } } = response;
  return { orders };
}

export async function startOrder(orderId) {
  const response = await makeRequest('post', `orders/${orderId}/start`);
  return response
}

export async function completeOrder(orderId) {
  return makeRequest('post', `orders/${orderId}/complete`);
}