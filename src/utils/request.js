import axios from 'axios';
import { getData, clearStorage } from 'src/utils/storage';

const request = axios.create({
  baseURL: 'http://aoo.mictronicx.com/api',
  timeout: 5000,
});

request.interceptors.request.use(async (config) => {
  config.headers.Authentication = await getData('auth_token');
  config.headers['content-type'] = 'application/json';
  return config;
});

request.interceptors.response.use(
  (response) => {
    console.log(response, 'network success');

    return response.data;
  },
  function(error) {
    console.log({ error }, 'network error');

    if (error?.response?.status === 403) {
      clearStorage();
    }

    return Promise.reject(error.response || error);
  },
);

export const multipartHeaders = {
  headers: {
    'Content-Type': 'multipart/formdata',
  },
};

export function makeRequest(method, url, data, headers) {
  console.log({ url, data });
  return new Promise((resolve, reject) => {
    return request[method](url, data, {
      headers,
    })
      .then(resolve)
      .catch((ex) => {
        reject(ex);
        console.log(ex, 'network error');
      });
  });
}

export { request };
