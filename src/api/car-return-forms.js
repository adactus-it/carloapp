import { request, toFormData, makeRequest, imageToFormData } from 'src/utils';

const getCollectionUrl = (orderId) => `orders/${orderId}/car_return_forms`;
const getPdfUrl = (orderId) => `car_return_pdf/${orderId}`;

export function getCarReturnFormForOrder(orderId) {
  return request({
    url: getCollectionUrl(orderId),
  });
}

export function getCarReturnFormPdf(orderId) {
  return makeRequest('get', getPdfUrl(orderId));
}

export function updateCarReturnFormForOrder(orderId, data) {
  console.log({ data, orderId });
  return makeRequest('post', getCollectionUrl(orderId), toFormData(data), {
    headers: {
      'Content-Type': 'application/json',
    },
  });
}

export function updateCarReturnTechnicalDefects(orderId, data) {
  let dummyData = {
    technical_defects: JSON.stringify(data),
  };
  return makeRequest('post', `orders/${orderId}/technical_defects`, toFormData(dummyData), {
    headers: {
      'Content-Type': 'application/json',
    },
  });
}

export function updateCarReturnFormCarIssues(carReturnFormId, data) {
  let formdata = new FormData();
  formdata.append('car_issues', JSON.stringify(data.car_issues));
  formdata.append('issues_snapshot', JSON.stringify(data.issues_snapshot));

  return makeRequest('post', `car_return_forms/${carReturnFormId}/car_issues`, formdata, {
    headers: {
      'content-type': 'application/json',
    },
  });
}

export function updateCarReturnFormMedia(carReturnFormId, data) {
  data = imageToFormData('media', data);
  return makeRequest('post', `car_return_forms/${carReturnFormId}/media`, data, {
    headers: {
      'content-type': 'application/json',
    },
  });
}
