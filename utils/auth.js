import AsyncStorage from '@react-native-community/async-storage'
export const USER_KEY = 'car-log-data'

export const onSignIn = (object) => AsyncStorage.setItem(USER_KEY, JSON.stringify(object))
export const getLoginData = () => AsyncStorage.getItem(USER_KEY)
export const onSignOut = () => AsyncStorage.removeItem(USER_KEY)