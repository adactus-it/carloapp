import React from 'react';
import PropTypes from 'prop-types';
import { getDisplayName } from './hooks';

function WithHeaderLeft(LeftComponent) {
  return function (WrappedComponent) {
    const updatedComponent = class extends React.Component {
      componentDidMount() {
        this.props.navigation.setOptions({
          headerLeft: () => <LeftComponent />,
        });
      }
      render() {
        return <WrappedComponent {...this.props}>{this.props.children}</WrappedComponent>;
      }
    };
    updatedComponent.displayName = `WithHeaderLeft(${getDisplayName(WrappedComponent)})`;
    updatedComponent.propTypes = {
      children: PropTypes.node.isRequired,
      navigation: PropTypes.object.isRequired,
    };
    return updatedComponent;
  };
}

export { WithHeaderLeft };
