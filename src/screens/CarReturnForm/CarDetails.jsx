import React from 'react';
import PropTypes from 'prop-types';

import { Text, HStack, VStack, Checkbox } from 'native-base';
import { withTranslation } from 'react-i18next';
import { Button, FormControl } from 'src/components/molecules';
import { useNavigation } from '@react-navigation/native';
import { useForm } from 'react-hook-form';

function CarDetails({ t, onSubmit, order }) {
  const navigation = useNavigation();

  const {
    control,
    handleSubmit,
  } = useForm({
    defaultValues: {},
  });

  const parseValues = (values) => {
    const dto = {};
    const { checkboxes } = values;

    if (checkboxes && checkboxes.length > 1) for (const val of checkboxes) dto[val] = true;
    delete values.checkboxes;
    const sent = { ...values, ...dto };
    onSubmit(sent);
  };

  return (
    <VStack space={2} mx={4}>
      <Text>Vehicle type: {order?.vehicle_type || 'N/A'}</Text>
      <Text>
        Return remarks: {order?.return_remark || 'N/A'}
      </Text>
      <Text>VIN: {order?.VIN || 'N/A'}</Text>
      <FormControl control={control} name='checkboxes' block>
        {({ field: { onChange } }) => (
          <Checkbox.Group accessibilityLabel='test' onChange={onChange} w={'100%'}>
            <Checkbox my={0.5} value={'motor_vehicle_registration'} accessibilityLabel='test'>
              Motor vehicle registration
            </Checkbox>
            <Checkbox my={0.5} value={'operation_manual'} accessibilityLabel='test'>
              Operation Manual
            </Checkbox>
            <Checkbox my={0.5} value={'spare_wheel'} accessibilityLabel='test'>
              Spare Wheel
            </Checkbox>
            <Checkbox my={0.5} value={'service_booklet'} accessibilityLabel='test'>
              Service booklet
            </Checkbox>
            <Checkbox my={0.5} value={'engine_oil_level_ok'} accessibilityLabel='test'>
              Engine oil level OK
            </Checkbox>
            <Checkbox my={0.5} value={'keys'} accessibilityLabel='test'>
              Keys
            </Checkbox>
          </Checkbox.Group>
        )}
      </FormControl>
      <FormControl
        label={t('Accessories')}
        placeholder='Enter missing vehicle details here'
        control={control}
        name={'special_equipment'}
        type='textarea'
        h={20}
        block
      />
      <FormControl
        control={control}
        name={'remarks'}
        label='Remarks'
        placeholder='Enter remarks here'
        type='textarea'
        h={20}
        block
      />
      <HStack justifyContent='space-around' w='100%' my={4}>
        <Button
          text={t('Save Order')}
          onPress={() => {
            navigation.navigate('HomeScreen');
          }}
        />
        <Button text={t('Continue')} onPress={handleSubmit(parseValues)} />
      </HStack>
    </VStack>
  );
}

CarDetails.propTypes = {
  t: PropTypes.func,
  navigation: PropTypes.object,
  onSubmit: PropTypes.func,
  order: PropTypes.object,
};

export default withTranslation()(CarDetails);
