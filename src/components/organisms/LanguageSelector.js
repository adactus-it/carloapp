import React from 'react';
import { Modal } from 'react-native';
import PropTypes from 'prop-types';

function LanguageSelector({ modalVisible, setModalVisible }) {
  return (
    <Modal
      animationType='slide'
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        setModalVisible(!modalVisible);
      }}
    ></Modal>
  );
}

LanguageSelector.propTypes = {
  modalVisible: PropTypes.bool,
  setModalVisible: PropTypes.func,
};
