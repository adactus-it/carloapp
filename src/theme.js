import { Dimensions, StyleSheet } from 'react-native';
import { extendTheme } from 'native-base';

const theme = {
  colors: {
    primary: '#23619C',
  },
};
export const vh = Dimensions.get('screen').height * 0.01; //to use this for responsive fonts scaling
export const vw = Dimensions.get('screen').width * 0.01; //to use this for responsive fonts scaling

const NBTheme = extendTheme({
  colors: {
    primary: {
      500: '#23619C',
    },
  },
  components: {
    Input: {
      baseStyle: {
        borderColor: '#ccc',
        borderBottomColor: '#000',
        borderWidth: 1,
      },
    },
  },
});

const styles = StyleSheet.create({
  fullWidth: {
    width: '100%',
  },
});

export { theme, styles, NBTheme };
