import React from 'react';
import PropTypes from 'prop-types';
import { Box, Spinner } from 'native-base';

export function SwitchComponent({ active, children }) {
  return active ? <Box>{children.filter((c) => `${c.key}` === `${active}`)}</Box> :
    <Spinner size={'lg'} color={'primary.500'} />;
}

SwitchComponent.propTypes = {
  active: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  loading: PropTypes.bool,
};

SwitchComponent.defaultProps = {
  loading: true,
};
