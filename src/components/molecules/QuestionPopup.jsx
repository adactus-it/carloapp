import { StyleSheet, Text, View, Modal, Pressable } from 'react-native';
import React, { useState } from 'react';
import { Radio, Image } from 'native-base';
import InputField from './InputField';
import ImagePicker from 'react-native-image-crop-picker';

const QuestionPopup = ({ popupVisible, selectedOption, onPopupSubmit, onImagePopupsubmit, setPopupVisible }) => {
  const [value, setValue] = useState();

  const openCamera = () =>
    ImagePicker.openCamera({
      cropping: true,
      compressImageQuality: 0.8,
    }).then(({ path, mime }) => {
      setPhotos({
        path,
        mime,
      });
    });

  return (
    <Modal
      animationType='slide'
      transparent={true}
      visible={popupVisible}
      onRequestClose={() => {
        setPopupVisible(!popupVisible);
      }}
    >
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          {selectedOption?.action?.popup?.inputType == 'text' && (
            <InputField onChangeText={setValue} placeholder={selectedOption?.action?.popup?.inputLabel} />
          )}
          {selectedOption?.action?.popup?.inputType == 'image' && (
            <Pressable
              style={[styles.imageContainer]}
              onPress={() => {
                openCamera();
              }}
            >
              {value ? (
                <Image source={{ uri: value.path }} h={'100%'} w={'100%'} resizeMode='cover' alt={`Photo: 1`} />
              ) : (
                <Text>Add image of defect</Text>
              )}
            </Pressable>
          )}
          {selectedOption?.action?.inputType == 'select' && (
            <View style={styles.radioContainer}>
              <Text>{selectedOption?.action?.inputLabel}</Text>
              <Radio.Group
                accessibilityLabel='test'
                justifyContent='space-around'
                alignItems={'flex-start'}
                name={selectedOption?.action?.inputLabel}
                onChange={setValue}
                value={value}
              >
                {selectedOption?.action?.inputOptions.map((it) => (
                  <Radio accessibilityLabel='test' key={it} value={it} mx={1} my={0.5}>
                    {it}
                  </Radio>
                ))}
              </Radio.Group>
            </View>
          )}

          <Pressable
            style={[styles.button, styles.buttonClose]}
            onPress={() => {
              if (selectedOption?.action?.popup?.inputType == 'image') {
                setPopupVisible(false);
                onImagePopupsubmit(value);
                setValue(false);
              } else {
                setPopupVisible(false);
                onPopupSubmit(value);
              }
            }}
          >
            <Text style={styles.textStyle}>Submit</Text>
          </Pressable>

          <Pressable
            style={[styles.button, styles.buttonClose]}
            onPress={() => {
              setPopupVisible(false);
            }}
          >
            <Text style={styles.textStyle}>Cancel</Text>
          </Pressable>
        </View>
      </View>
    </Modal>
  );
};

export default QuestionPopup;

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    width: '90%',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    paddingHorizontal: 50,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
    marginVertical: 5,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  radioContainer: {
    width: '100%',
    marginBottom: 10,
  },
  imageContainer: {
    borderWidth: 1,
    borderColor: 'lightgrey',
    height: 150,
    width: '70%',
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 3,
    overflow: 'hidden',
  },
});
