/* eslint-disable react/prop-types */
import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import { GLOBAL_STYLES } from '../../utils/constants';
// import { USER_KEY } from '../../utils/auth'

class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  async _bootstrapAsync() {
    // const userToken = await AsyncStorage.getItem(USER_KEY)
    const { props } = this;
    setTimeout(() => {
      props.navigation.replace('Login');
      // props.navigation.navigate('DriverSchedule')
      // props.navigation.navigate(userToken ? 'Home' : 'Login')
    }, 500);
  }

  render() {
    GLOBAL_STYLES;
    return (
      <View style={GLOBAL_STYLES.container}>
        <ActivityIndicator />
      </View>
    );
  }
}
export default AuthLoadingScreen;
