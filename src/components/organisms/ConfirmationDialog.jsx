import { Alert } from 'react-native';

function ConfirmationDialog(description, handleConfirm, title, confirmText, deleteText, handleDelete) {
  return Alert.alert(title || 'Are you sure?', description, [
    {
      text: deleteText || 'Cancel',
      onPress: handleDelete || (() => console.log('Cancel Pressed')),
      style: 'cancel',
    },
    { text: confirmText || 'Yes', onPress: handleConfirm },
  ]);
}

export { ConfirmationDialog };
