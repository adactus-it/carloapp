import { StyleSheet } from 'react-native'
// import { WToast, WModal } from 'react-native-smart-tip'
// import { create } from 'handlebars'

export const BASE_API_URL = 'https://radiant-everglades-85723.herokuapp.com/api/'
export const BASE_EXTERNAL_URL = 'https://radiant-everglades-85723.herokuapp.com/public/'
// export const BASE_API_URL = "http://localhost:5000/api/"
// export const BASE_EXTERNAL_URL = "http://localhost:5000/public/"

export const LOGIN_ROUTE = 'r_login'
export const REGISTER_ROUTE = 'r_register'

export const GLOBAL_STYLES = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
})

// Base
// show = () => {
//   WToast.show({ data: 'hello world' })
// }

// Other
// export const _showToast = (_message) => {
//     const modalOpts = {
//         data: _message,
//         textColor: 'black',
//         backgroundColor: 'whitesmoke',
//         position: WModal.position.BOTTOM
//     }
//     WToast.show(modalOpts)
// }

// export const _showWarning = (_message) => {
//     const modalOpts = {
//         data: _message,
//         textColor: 'white',
//         backgroundColor: '#fca903',
//         position: WModal.position.BOTTOM
//     }
//     WToast.show(modalOpts)
// }

// export const _showError = (_message) => {
//     const modalOpts = {
//         data: _message,
//         textColor: 'white',
//         backgroundColor: '#d8242f',
//         position: WModal.position.BOTTOM
//     }
//     WToast.show(modalOpts)
// }

// export const _showSuccess = (_message) => {
//     const modalOpts = {
//         data: _message,
//         textColor: 'white',
//         backgroundColor: '#02bdb6',
//         position: WModal.position.BOTTOM
//     }
//     WToast.show(modalOpts)
// }

export const _globalStyles = StyleSheet.create({
  textInputStyle: {
    textAlign: 'center',
    marginBottom: 7,
    margin: 12,
    height: 40,
    backgroundColor: '#404040',
    borderRadius: 6,
    color: 'white',
  },
})

export const basketActions = {
  ADD_TO_BASKET: 'ADD_TO_BASKET',
  REMOVE_FROM_BASKET: 'REMOVE_FROM_BASKET',

  ADD_BASKET: 'ADD_BASKET',
  REMOVE_BASKET: 'REMOVE_BASKET',
  GET_BASKETS: 'GET_BASKETS',
  UPDATE_BASKETS: 'UPDATE_BASKETS',

  SET_CURR_BASKET: 'SET_CURR_BASKET',
  GET_CURR_BASKET: 'GET_CURR_BASKET',
}
export const locationActions = {
  SET_LOCATION: 'SET_LOCATION',
  GET_LOCATION: 'GET_LOCATION',
}
export const userActions = {
  SET_USER: 'SET_USER',
}
export const cartActions = {
  ADD_TO_CART: 'ADD_TO_CART',
  REMOVE_FROM_CART: 'REMOVE_FROM_CART',
}
